<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>

    <meta name="keywards" content="11">
    <meta name="description" content="1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,400italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="css/style.css">
    <title>Index</title>


</head>
<body>
<header>

    <nav class="page-navigation">
        <div class="page-navigation__logo logo page-logo">
            <%--<img src="img/icons/logo.png" alt="Movie House">--%>
        </div>

        <ul class="page-navigation__list">
            <li class="page-navigation__list-icon page-navigation__list-icon--active"><a href="#">Скоро в прокате</a></li>
            <li class="page-navigation__list-icon"><a href="#">Афиша</a></li>
            <li class="page-navigation__list-icon"><a href="${pageContext.servletContext.contextPath}/movie-houses">Кинотеатры</a></li>
            <li class="page-navigation__list-icon"><a href="#">Трейлеры</a></li>
            <li class="page-navigation__list-icon"><a href="#">Обсуждение</a></li>
        </ul>

        <div class="user-block page-navigation__user-block">
            <a class="user-block__basket" href="#">Войти</a>
            <a class="user-block__basket" href="#">Регистрация</a>
        </div>


    </nav>
</header>
<main>
    <div class="page-center">

        <%--<ul><c:redirect url="/Main"/>--%>
            <c:forEach items="${movieDTOs}" var="movies">
                <div>
                    <img src="img/inons/${movies.icon}" width="300px" height="300px" alt="icon">
                </div>

                <div>
                    <a href="${pageContext.servletContext.contextPath}/movie?id=${movies.id}">${movies.name}</a>
                </div>
                <div>
                    <a href="${pageContext.servletContext.contextPath}/movie-houses?movie_id=${movies.id}">Где посмотреть</a>
                </div>
                <div>

                </div>
            </c:forEach>

        </ul>
    </div>
</main>
</body>
</html>

