<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 09.04.2017
  Time: 0:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<c:import url="../templates/header.jsp">
</c:import>
<c:if test="${done eq 0}">
<c:forEach items="${ticketList}" var="ticket">
    <div><c:out value="${currentMovieHouse.name}"></c:out></div>
    <div><c:out value="${currentHall.name}"></c:out></div>
    <div><c:out value="${date}"></c:out></div>
    <div><c:out value="${currentSeance.currentTime}"></c:out></div>

    <div><span>Ряд: </span><c:out value="${ticket.seatRow}"></c:out></div>
    <div><span>Место: </span><c:out value="${ticket.seatPosition}"></c:out></div>
    <div><c:out value="${currentSeance.price}"></c:out></div>
    <div><c:out value="${user.userName}"></c:out></div>
</c:forEach>
<form name="sellForm" action="${pageContext.servletContext.contextPath}/sell" method="post">
    <input name="success" type="checkbox" checked="checked" hidden="hidden">
    <input type="submit" value="Оплата">
</form>
</c:if>
<c:if test="${done eq 1}">
    <p>Поздравляем с приобретением билета. Ждем Вас с нетерпением!</p>
</c:if>
<c:import url="../templates/modal-content.jsp">
</c:import>
</body>
</html>
