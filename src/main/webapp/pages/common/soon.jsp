<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 14.04.2017
  Time: 16:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<c:import url="../templates/header.jsp">
</c:import>
<main>
    <div class="page-center">
        <div>
            <span>Сегодня: </span>
            <span><c:out value="${date}"></c:out></span>
        </div>
        <p>Выберите дату</p>
        <div>
            <form name="dateSelection" action="${pageContext.servletContext.contextPath}/Main" method="POST">
                <input name="date" type="date" placeholder="${date}">
                <input type="submit" name="submit" value="Установить">
            </form>
        </div>
        <ul>
            <c:forEach items="${movieDTOs}" var="movies">
                <div>
                    <img src="img/icons/${movies.icon}" width="300px" height="300px" alt="icon">
                </div>

                <div>
                    <a href="${pageContext.servletContext.contextPath}/movie?id=${movies.id}">${movies.name}</a>
                </div>
                <%--<div>--%>
                    <%--<a href="${pageContext.servletContext.contextPath}/movie-houses?movie_id=${movies.id}">Где--%>
                        <%--посмотреть</a>--%>
                <%--</div>--%>
                <div>

                </div>
            </c:forEach>

        </ul>
    </div>
</main>
<c:import url="../templates/modal-content.jsp">
</c:import>
</body>
</html>
