<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 03.04.2017
  Time: 22:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<c:import url="../templates/header.jsp">
    </c:import>
<main>
    <div class="page-center">

        <ul>
            <c:forEach items="${movieHouseAll}" var="movieHouse">

                <li>
                    <div>
                    <a href="${pageContext.servletContext.contextPath}/current-movie-house?movie-house=${movieHouse.id}">${movieHouse.name}</a>
                    </div>

                </li>
            </c:forEach>

        </ul>
    </div>
</main>


<c:import url="../templates/modal-content.jsp">
</c:import>
</body>
</html>
