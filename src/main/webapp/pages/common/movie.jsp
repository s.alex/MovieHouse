<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<c:import url="../templates/header.jsp">
</c:import>
<main>
    <div class="page-center">
<div>
    <%--${pageContext.servletContext.contextPath}/img/inons/${movieDTO.icon}--%>
    <div>
        <img src="${pageContext.servletContext.contextPath}/img/icons/${movieDTO.icon}" alt="icon">
    </div>
    <div>
        <c:out value="${movieDTO.name}" default="err"/>
    </div>
    <div>
        <c:out value="${movieDTO.genre}" default="err"/>
    </div>
    <div>
        <c:out value="${movieDTO.producedBy}" default="err"/>
    </div>
    <div>
        <c:out value="${movieDTO.duration}" default="err"/>
    </div>
    <div>
        <c:out value="${movieDTO.description}" default="err"/>
    </div>
    <div>
        <c:out value="${movieDTO.rating}" default="err"/>
    </div>

    <div>
        <c:out value="${movieDTO.actors}" default="err"/>
    </div>
        <div>
            <a href="${pageContext.servletContext.contextPath}/movie-houses?movie_id=${movieDTO.id}">Где посмотреть</a>
        </div>
</div>

        </ul>
    </div>
</main>
<footer>

</footer>
<c:import url="../templates/modal-content.jsp">
</c:import>
</body>
</html>