<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 05.04.2017
  Time: 22:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<c:import url="../templates/header.jsp">
</c:import>
<section class="advertising">
<c:forEach items="${hallAllByMovieHouseId}" var="hall">
    <div class="advertising__item advertising__item--slim" width="80%" align="center" >
        <div class="" width="49%" float="left">
            <p><span>Название зала </span><span>${hall.name}</span></p>
            <p><span>Колличество мест в зале </span>${hall.seatsQuantity}</p>
            <p><span>Колличество рядов </span>${hall.rows}</p>
            <p><span>Колличество мест в ряде </span>${hall.rowSeats}</p>
        </div>
        <div class="advertising__item advertising__item--large" width="49%" float="right">
            <ul>
                <c:forEach items="${seanceAll}" var="seance">
                    <c:if test="${hall.id==seance.hallId}">
                        <li>
                            <div>
                                 <p><span>Название фильма </span><span>${seance.movieTitle}</span></p>
                            </div>
                            <div>
                                  <p><span>Начало сеанса </span><span>${seance.currentTime}</span></p>
                            </div>
                            <div>
                                   <p> <span>Цена билета </span><span>${seance.price}</span></p>
                            </div>
                            <div>
                                <p> <a href="${pageContext.servletContext.contextPath}/seats?seance_id=${seance.id}">Наличие мест</a></p>
                            </div>
                        </li>

                    </c:if>

                </c:forEach>
            </ul>
        </div>
    </div>
</c:forEach>
</section>
<c:import url="../templates/modal-content.jsp">
</c:import>
</body>
</html>
