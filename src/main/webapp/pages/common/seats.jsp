<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 08.04.2017
  Time: 0:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<c:import url="../templates/header.jsp">
</c:import>
<main>
    <div class="page-center">
        <table>

            <tr>
                <th>&nbsp;</th>
                <c:forEach var="seat" begin="0" end="${currentHall.rowSeats-1}" step="1" varStatus="currentSeat">
                    <th>
                        <p>Место: </p> <c:out value="${seat+1}"/>
                    </th>
                </c:forEach>
            </tr>
            <c:forEach var="row" begin="0" end="${currentHall.rows-1}" step="1" varStatus="currentRow">
                <tr>
                    <th>
                        Ряд: <c:out value="${row+1}"/>
                    </th>
                    <c:forEach var="seat" begin="0" end="${currentHall.rowSeats-1}" step="1" varStatus="currentSeat">
                        <td>


                            <c:if test="${structureHall[row][seat] eq 0}">
                                <label> <c:out value="${seat+1}"/></label>
                            </c:if>
                            <c:if test="${structureHall[row][seat] ne 0}">
                                <a class="seat"
                                   href="${pageContext.servletContext.contextPath}/seats?seance_id=${currentSeance.id}&row=${row+1}&seat=${seat+1}">
                                    <c:out value="${seat+1}"/></a>
                            </c:if>
                        </td>
                    </c:forEach>
                </tr>
            </c:forEach>
        </table>
        <label><c:out value="${user.userName}"/></label>
        <form name="seance_info" action="${pageContext.servletContext.contextPath}/sell" method="post">

            <c:if test="${user.userName ne null}">
                <input name="submit" type="submit" value="Купить билет"></c:if><label> Доступно только зарегистрированым
            пользователям</label>
        </form>
        <div><span>У Вас в корзине билетов: </span> <span>
    <c:if test="${ticketList ne null}">
        <c:out value="${size}"/>
    </c:if>
    <c:if test="${ticketList eq null}">0</c:if>
 </span>
        </div>
    </div>
</main>
<c:import url="../templates/modal-content.jsp">
</c:import>
<script src="seat-action.js"></script>
</body>
</html>
