<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 09.04.2017
  Time: 0:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<c:import url="../templates/header.jsp">
</c:import>
<main>
    <div class="page-center">


        <form class="modal-content__form" accept-charset="UTF-8"
              action="${pageContext.servletContext.contextPath}/registration" method="POST">
            <label class="modal-content__label">Ваше Имя:<br>
                <input class="user-name" type="text" name="name" placeholder="Ваше Имя" required>
            </label>
            <label class="modal-content__label">Ваша Фамилия:<br>
                <input class="user-name" type="text" name="surname" placeholder="Ваша Фамилия" required>
            </label>
            <label class="modal-content__label">Логин:<br>
                <input class="user-name" type="text" name="login" placeholder="Логин" required>
            </label>
            <label class="modal-content__label">Пароль:<br>
                <input class="user-name" type="password" name="password" placeholder="Пароль" required>
            </label>
            <label class="modal-content__label">Повторите пароль:<br>
                <input class="user-name" type="password" name="confirm-password" placeholder="Повторите пароль"
                       required>
            </label>
            <label class="modal-content__label">Ваш e-mail:<br>
                <input class="user-email" type="email" name="email" placeholder="Ваш e-mail:" required>
            </label>

            <%--<input hidden="hidden" type="text" name="permissions" value="0" required>--%>

            <button class="btn btn--largest btn--pink" type="submit">Регистрация</button>
        </form>
    </div>
</main>
<c:import url="../templates/modal-content.jsp">
</c:import>
<script src="js/registration.js"></script>
</body>
</html>
