<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 15.04.2017
  Time: 22:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<c:import url="../templates/header.jsp">
</c:import>
<main>
    <c:out value="${message}"></c:out>
    <div class="page-center">
        <p>Изменить персональные данные</p>
        <form name="userConfig" action="${pageContext.servletContext.contextPath}/personal-area" method="post">
            <label>Имя <input name="userName" type="text" value="${userByLogin.userName}" required></label>
            <label>Фамилия <input name="userSurname" type="text" value="${userByLogin.userSurname}" required></label>
            <label>Login <input name="userLogin" type="text" value="${userByLogin.userLogin}" required></label>
            <label>E-mail <input name="userEmail" type="text" value="${userByLogin.userEmail}" required></label>

            <input type="submit" value="Изменить ">
        </form>
        <p>Дисконтная программа</p>
        <label>Ваш Дисконт= <c:out value="${userByLogin.userDiscount}"></c:out>%</label>

    <p>Изменить пароль</p>
    <form name="changePassword" action="${pageContext.servletContext.contextPath}/personal-area" method="post">
        <p>Старый пароль <input name="oldPassword" type="password" required></p>
        <p>Новый пароль <input name="newPassword" type="password" required></p>
        <p>Повторите пароль <input name="confirmPassword" type="password" required></p>
        <input type="submit" value="Изменить пароль">
    </form>
    </div>
</main>
</body>
</html>
