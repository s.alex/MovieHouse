<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 07.04.2017
  Time: 15:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<div class="modal-content ">
    <button class="modal-content__close" type="button" title="Close">Close</button>

    <form class="modal-content__form" action="${pageContext.servletContext.contextPath}/Main" method="POST">

        <label class="modal-content__label">Логин:<br>
            <input class="user-name" type="text" name="login" placeholder="Логин" required>
        </label>
        <label class="modal-content__label">Пароль:<br>
            <input class="user-name" type="password" name="password" placeholder="Пароль" required>
        </label>


        <button class="btn btn--largest btn--pink" type="submit">Вход</button>
    </form>
</div>
<div class="overlay"></div>
<div class="page-wrapper"></div>
<script src="js/slider-action.js"></script>
<script src="js/modal-content.js"></script>
<script src="js/page-navigation.js"></script>