<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 04.04.2017
  Time: 23:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>

    <meta name="keywards" content="11">
    <meta name="description" content="1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,400italic&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="css/style.css">
    <title>Кинотеатры</title>
</head>
<body>
<header>

    <nav class="page-navigation">
        <div class="page-navigation__logo logo page-logo">
            <img src="img/icons/logo.png" alt="Movie House">
        </div>

        <ul class="page-navigation__list">
            <li class="page-navigation__list-icon "><a href="${pageContext.servletContext.contextPath}/soon">Скоро в прокате</a>
            </li>
            <li class="page-navigation__list-icon page-navigation__list-icon--active"><a href="${pageContext.servletContext.contextPath}/Main">Афиша</a></li>
            <li class="page-navigation__list-icon"><a
                    href="${pageContext.servletContext.contextPath}/movie-houses">Кинотеатры</a></li>
            <%--<li class="page-navigation__list-icon"><a href="#">Трейлеры</a></li>--%>
            <%--<li class="page-navigation__list-icon"><a href="#">Обсуждение</a></li>--%>
        </ul>

        <div class="user-block page-navigation__user-block">
            <c:if test="${userByLogin eq null}">

            </c:if>
            <c:choose>
                <c:when test="${userByLogin eq null}">
                    <a class="user-block__basket js-mail" href="#">Войти</a>
                    <a class="user-block__basket " href="${pageContext.servletContext.contextPath}/registration">Регистрация</a>
                </c:when>
                <c:when test="${userByLogin ne null}">
                    <p>Здравствуйте, <c:out value="${userByLogin.userLogin}"></c:out></p>
                    <a class="user-block__basket " href="${pageContext.servletContext.contextPath}/personal-area">Личный кабинет</a>
                    <%--<a class="user-block__basket " href="${pageContext.servletContext.contextPath}/basket">Корзина</a>--%>
                    <a class="user-block__basket " href="${pageContext.servletContext.contextPath}/Main?exit=1">Выход</a>
                </c:when>
                <c:otherwise>
                    <a class="user-block__basket " href="#">Войти</a>
                    <a class="user-block__basket " href="${pageContext.servletContext.contextPath}/registration">Регистрация</a>
                </c:otherwise>
            </c:choose>

        </div>


    </nav>
</header>
