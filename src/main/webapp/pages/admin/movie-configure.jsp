<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 12.04.2017
  Time: 1:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="keywards" content="11">
    <meta name="description" content="1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,400italic&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="../css/style.css">
    <title>Movie Configure</title>
</head>
<body>
<div class="page-center">
    <nav class="page-navigation">
        <ul class="page-navigation__list large">
            <li class="page-navigation__list-icon ">
                <a href="${pageContext.servletContext.contextPath}/admin/admin-configure">Конфигурация
                    администраторов</a>
            </li>
            <li class="page-navigation__list-icon ">
                <a href="${pageContext.servletContext.contextPath}/admin/movie-house-configure">Конфигурация
                    кинотеатра</a>
            </li>
            <li class="page-navigation__list-icon">
                <a href="${pageContext.servletContext.contextPath}/admin/seance-configure">Конфигурация Сеансов</a>
            </li>
            <li class="page-navigation__list-icon">
                <a href="${pageContext.servletContext.contextPath}/admin/movie-configure">Конфигурация фильмов</a>
            </li>
            <%--<li class="page-navigation__list-icon"><a href="#">Обсуждение</a></li>--%>
        </ul>
    </nav>
    <div>
        <p>
            <c:out value="${message}"></c:out>
        </p>
        <table>
            <tr>
                <th>
                    <p>Название фильма</p>
                </th>
            </tr>
            <c:forEach items="${movieDTOs}" var="movie">
                <tr>
                    <td>
                        <c:out value="${movie.name}"></c:out>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <p>Добавить новый фильм в базу данных</p>

    <form name="movieConfiguration" class="movieConfiguration" enctype="multipart/form-data" accept-charset="UTF-8"
          action="${pageContext.servletContext.contextPath}/admin/movie-configure" method="post">
        <label>Название фильма <input name="title" type="text" placeholder="Название фильма" required></label><br>
        <label>Жанр <input name="genre" type="text" placeholder="Жанр" required></label><br>
        <label>Режиссер <input name="producedBy" type="text" placeholder="Режиссер" required></label><br>
        <label>Длительность <input name="duration" type="text" placeholder="Длительность" required></label><br>
        <label>Рейтинг <input name="rating" type="text" placeholder="Рейтинг" required></label><br>
        <label>Страна <input name="country" type="text" placeholder="Страна" required></label><br>
        <%--<label>Постер <input name="icon" type="text" placeholder="Постер"></label><br>--%>
        <label><input name="file" class="file" type="file" accept="image/*,image/jpeg"></label>
        <%--<input type="text" hidden="hidden" name="filePath" class="filePath">--%>
        <label>В главных ролях <input class="" name="actors" type="text" placeholder="В главных ролях"></label><br>
        <label>
        <textarea name="description" cols="150" rows="20">

        </textarea>
        </label>
        <input class="submit" type="submit" value="Сохранить">

    </form>
</div>
<script src="../js/pathOfFile.js"></script>
</body>
</html>
