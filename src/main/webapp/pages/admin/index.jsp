<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 11.04.2017
  Time: 15:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>

    <meta name="keywards" content="11">
    <meta name="description" content="1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,400italic&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/style.css">
    <title>Admin</title>
</head>
<body>
<div class="page-center">
    <nav class="page-navigation">
        <ul class="page-navigation__list large">
            <li class="page-navigation__list-icon ">
                <a href="${pageContext.servletContext.contextPath}/admin/admin-configure">Конфигурация
                    администраторов</a>
            </li>
            <li class="page-navigation__list-icon ">
                <a href="${pageContext.servletContext.contextPath}/admin/movie-house-configure">Конфигурация
                    кинотеатра</a>
            </li>
            <li class="page-navigation__list-icon">
                <a href="${pageContext.servletContext.contextPath}/admin/seance-configure">Конфигурация Сеансов</a>
            </li>
            <li class="page-navigation__list-icon">
                <a href="${pageContext.servletContext.contextPath}/admin/movie-configure">Конфигурация фильмов</a>
            </li>
            <%--<li class="page-navigation__list-icon"><a href="#">Обсуждение</a></li>--%>
        </ul>
    </nav>
    <h1>Статистика продаж</h1>
    <p>Не реализовано</p>
</div>
</body>
</html>
