<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 11.04.2017
  Time: 17:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>

    <meta name="keywards" content="11">
    <meta name="description" content="1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,400italic&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="../css/style.css">
    <title>Admin Configure</title>
</head>
<body>
<div class="page-center">
    <nav class="page-navigation">
        <ul class="page-navigation__list large">
            <li class="page-navigation__list-icon ">
                <a href="${pageContext.servletContext.contextPath}/admin/admin-configure">Конфигурация
                    администраторов</a>
            </li>
            <li class="page-navigation__list-icon ">
                <a href="${pageContext.servletContext.contextPath}/admin/movie-house-configure">Конфигурация
                    кинотеатра</a>
            </li>
            <li class="page-navigation__list-icon">
                <a href="${pageContext.servletContext.contextPath}/admin/seance-configure">Конфигурация Сеансов</a>
            </li>
            <li class="page-navigation__list-icon">
                <a href="${pageContext.servletContext.contextPath}/admin/movie-configure">Конфигурация фильмов</a>
            </li>
            <%--<li class="page-navigation__list-icon"><a href="#">Обсуждение</a></li>--%>
        </ul>
    </nav>
    <div>Администраторы:</div>
    <table>
        <tr>
            <th>Фамилия</th>
            <th>Имя</th>
            <th>Логин</th>
            <th>e-mail</th>
        </tr>
        <c:forEach items="${userDTOList}" var="user">
            <tr>
                <td>
                    <c:out value="${user.userSurname}"></c:out>
                </td>
                <td>
                    <c:out value="${user.userName}"></c:out>
                </td>
                <td>
                    <c:out value="${user.userLogin}"></c:out>
                </td>
                <td>
                    <c:out value="${user.userEmail}"></c:out>
                </td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/admin/admin-configure?user=${user.id}">Удалить</a>
                </td>
            </tr>
        </c:forEach>
    </table>
    <div>
        Для того чтобы добавить пользователя заполните форму
    </div>
    <form name="adminConfiguration" accept-charset="UTF-8"
          action="${pageContext.servletContext.contextPath}/admin/admin-configure" method="post">
        <label>Фамилия <input name="surname" type="text" placeholder="Фамилия" required></label><br>
        <label>Имя <input name="name" type="text" placeholder="Имя" required></label><br>
        <label>Логин <input name="login" type="text" placeholder="Логин" required></label><br>
        <label>Пароль <input name="password" type="password" placeholder="Пароль" required></label><br>
        <label>Повторите пароль <input name="confirmPassword" type="password" placeholder="Пароль" required></label><br>
        <label>E-mail <input name="email" type="email" placeholder="E-mail" required></label><br>
        <label>Администратор <input name="permission" type="checkbox" checked=""></label>
        <input type="submit" value="Зарегистрировать">
    </form>
</div>
</body>
</html>
