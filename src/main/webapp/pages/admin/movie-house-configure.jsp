<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 12.04.2017
  Time: 1:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta name="keywards" content="11">
    <meta name="description" content="1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,400italic&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="../css/style.css">
    <title>Movie House Configure</title>
</head>
<body>
<div class="page-center">
    <nav class="page-navigation">
        <ul class="page-navigation__list large">
            <li class="page-navigation__list-icon ">
                <a href="${pageContext.servletContext.contextPath}/admin/admin-configure">Конфигурация
                    администраторов</a>
            </li>
            <li class="page-navigation__list-icon ">
                <a href="${pageContext.servletContext.contextPath}/admin/movie-house-configure">Конфигурация
                    кинотеатра</a>
            </li>
            <li class="page-navigation__list-icon">
                <a href="${pageContext.servletContext.contextPath}/admin/seance-configure">Конфигурация Сеансов</a>
            </li>
            <li class="page-navigation__list-icon">
                <a href="${pageContext.servletContext.contextPath}/admin/movie-configure">Конфигурация фильмов</a>
            </li>
            <%--<li class="page-navigation__list-icon"><a href="#">Обсуждение</a></li>--%>
        </ul>
    </nav>
    <div>Кинотеатры:</div>
    <div>
        <table>
            <tr>
                <th>Название</th>
                <th>Залы</th>

            </tr>
            <c:forEach items="${houseDTOs}" var="house">
                <tr>
                    <td>
                        <c:out value="${house.name}"></c:out>
                    </td>
                    <td>
                        <c:out value="${house.hallsQuantity}"></c:out>
                    </td>

                    <td>
                        <a href="${pageContext.servletContext.contextPath}/admin/movie-house-configure?house=${house.id}">Удалить</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <div>
        <p>Добавить новый кинотеатр</p>
        <form name="movieHouseConfig" class="movieHouseConfiguration" accept-charset="UTF-8"
              action="${pageContext.servletContext.contextPath}/admin/movie-house-configure" method="post">
            <label>Название <input name="houseTitle" type="text" placeholder="Название " required></label><br>

            <label>Колличество залов <input name="houseHalls" type="text" placeholder="Колличество залов"
                                            required></label><br>


            <input class="submit" type="submit" value="Сохранить">

        </form>

    </div>
    <div>Конфигурафия зала</div>

    <div>
        <form name="houseChoice" class="houseChoice"
              action="${pageContext.servletContext.contextPath}/admin/movie-house-configure" method="post">
            <p>Выберите кинотеатр для которого конфигурируется зал</p>
            <select name="houseId" class="houseId">
                <option value=""></option>
                <c:forEach items="${houseDTOs}" var="house">
                    <option value="${house.id}">
                        <c:out value="${house.name}"></c:out>
                    </option>
                </c:forEach>
            </select>
            <input type="submit" value="Выбрать">
        </form>
        <p>Уже имеющиеся залы</p>
        <table class="halls-output">
            <tr class="halls-header">
                <th>
                    Название
                </th>
                <th>
                    Ряды
                </th>
                <th>
                    Мест в ряде
                </th>
            </tr>
            <c:forEach items="${hallDTOs}" var="hall">
                <tr>
                    <td>
                        <c:out value="${hall.name}"></c:out>
                    </td>
                    <td>
                        <c:out value="${hall.rows}"></c:out>
                    </td>
                    <td>
                        <c:out value="${hall.rowSeats}"></c:out>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <div>Конфигурация зала</div>
        <form name="hallConfig" class="hallConfig"
              action="${pageContext.servletContext.contextPath}/admin/movie-house-configure" method="post">
            <label>Название зала<input name="hallName" placeholder="Название" required> </label><br>
            <label>Колличество рядов<input name="rows" placeholder="Колличество рядов" required> </label><br>
            <label>Колличество мест в ряде<input name="rowSeats" placeholder="Колличество мест в ряде" required>
            </label>
            <input name="houseId" hidden value="${houseId}">
            <input type="submit" value="Сохранить">
        </form>
    </div>
</div>
</body>
</html>
