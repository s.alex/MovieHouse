<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 12.04.2017
  Time: 1:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta name="keywards" content="11">
    <meta name="description" content="1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,400italic&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="../css/style.css">
    <title>Seance Configure</title>
</head>
<body>
<div class="page-center">
    <nav class="page-navigation">
        <ul class="page-navigation__list large">
            <li class="page-navigation__list-icon ">
                <a href="${pageContext.servletContext.contextPath}/admin/admin-configure">Конфигурация
                    администраторов</a>
            </li>
            <li class="page-navigation__list-icon ">
                <a href="${pageContext.servletContext.contextPath}/admin/movie-house-configure">Конфигурация
                    кинотеатра</a>
            </li>
            <li class="page-navigation__list-icon">
                <a href="${pageContext.servletContext.contextPath}/admin/seance-configure">Конфигурация Сеансов</a>
            </li>
            <li class="page-navigation__list-icon">
                <a href="${pageContext.servletContext.contextPath}/admin/movie-configure">Конфигурация фильмов</a>
            </li>
            <%--<li class="page-navigation__list-icon"><a href="#">Обсуждение</a></li>--%>
        </ul>
    </nav>
    <main>
        <div class="page-center">
            <div>
                <span>Дата: </span>
                <span>${date}</span>
            </div>
            <div>
            <form name="dateSelection" action="${pageContext.servletContext.contextPath}/admin/seance-configure" method="POST">
            <input name="date" type="date" placeholder="${date}">
            <input type="submit" name="submit" value="Установить">
            </form>
            </div>
            <ul>
                <c:forEach items="${movieDTOs}" var="movies">
                    <%--<div>--%>
                    <%--<img src="img/icons/${movies.icon}" width="300px" height="300px" alt="icon">--%>
                    <%--</div>--%>

                    <li>
                        <span>Название:  </span> <a
                            href="${pageContext.servletContext.contextPath}/movie?id=${movies.id}">${movies.name}</a>
                            <%--</div>--%>
                            <%--<div>--%>
                            <%--<a href="${pageContext.servletContext.contextPath}/movie-houses?movie_id=${movies.id}">Где--%>
                            <%--посмотреть</a>--%>
                            <%--</div>--%>
                            <%--<div>--%>

                    </li>
                </c:forEach>

            </ul>
            <p>Все сеансы cегодня</p>
            <table>
                <tr>
                    <th>
                        <span>id</span>
                    </th>
                    <th>
                        <span>Фильм</span>
                    </th>
                    <th>
                        <span>Кинотеатр</span>
                    </th>
                    <th>
                        <span>Зал</span>
                    </th>
                    <th>
                        <span>День начала показа</span>
                    </th>
                    <th>
                        <span>День окончания показа</span>
                    </th>
                    <th>
                        <span>Время показа</span>
                    </th>
                    <th>
                        <span>Цена</span>
                    </th>
                </tr>
                <c:forEach items="${allSeancesByDate}" var="seances">


                    <tr>
                        <td><c:out value="${seances.id}"></c:out></td>

                        <td>
                            <c:set var="movieMapName" scope="page" value="${seances.movieId}"></c:set>
                            <c:out value="${movieMap[movieMapName]}"></c:out>
                        </td>

                        <td>
                            <c:set var="houseName" scope="page" value="${seances.movieHouseId}"></c:set>
                            <c:out value="${houseMap[houseName]}"></c:out>
                        </td>

                        <td>
                            <c:set var="hallName" scope="page" value="${seances.hallId}"></c:set>
                            <c:out value="${hallMap[hallName]}"></c:out>
                        </td>

                        <td><c:out value="${seances.dayBegin}"></c:out></td>

                        <td><c:out value="${seances.dayEnd}"></c:out></td>

                        <td>
                            <c:set var="time" scope="page" value="${seances.currentSeanceTime}"></c:set>
                            <c:out value="${timeMap[time]}"></c:out>
                        </td>

                        <td><c:out value="${seances.price}"></c:out></td>
                    </tr>
                </c:forEach>

            </table>
            <div>
                <p>Конфигурация сеанса</p>
                <div>
                    <label>
                        Выберите кинотеатр
                    </label>

                    <c:if test="${movieHouse eq null}">
                        <form name="movieHouseChoice" class="movieHouseChoice"
                              action="${pageContext.servletContext.contextPath}/admin/seance-configure" method="post">
                            <select name="movieHouse">
                                <option value=""></option>
                                <c:forEach items="${movieHouseDTOs}" var="house">
                                    <option value="${house.id}">

                                        <c:out value="${house.name}"/>
                                    </option>
                                </c:forEach>
                            </select>
                            <input type="submit" value="Сохранить">
                        </form>
                    </c:if>
                    <c:if test="${movieHouse ne null}">
                        <span>Выбран: </span>
                        <%--<c:set var="houseChoice" scope="page" value="${movieHouse}"></c:set>--%>
                        <span> <c:out value="${houseMap[movieHouse]}"/> </span>

                    </c:if>
                </div>
                <div>
                    <label>
                        Выберите зал
                    </label>
                    <c:if test="${movieHall eq null}">
                        <form name="movieHallChoice" class="movieHallChoice"
                              action="${pageContext.servletContext.contextPath}/admin/seance-configure" method="post">

                            <select name="movieHall">
                                <option value=""></option>
                                <c:forEach items="${hallDTOs}" var="hall">

                                    <option value="${hall.id}">
                                        <c:out value="${hall.name}"/>
                                    </option>

                                </c:forEach>
                            </select>

                            <input type="submit" value="Сохранить">
                        </form>
                    </c:if>
                    <c:if test="${movieHall ne null}">
                        <span>Выбран: </span>

                        <span> <c:out value="${hallMap[movieHall]}"/> </span>
                    </c:if>
                </div>
                <div>
                    <label>
                        Выберите время
                    </label>
                    <c:if test="${currentTime eq null}">
                        <form name="currentTimeChoice" class="currentTimeChoice"
                              action="${pageContext.servletContext.contextPath}/admin/seance-configure" method="post">

                            <select name="currentTime">
                                <c:forEach items="${timesDTOs}" var="time">
                                    <option value="${time.id}">
                                        <c:out value="${time.time}"/>
                                    </option>
                                </c:forEach>
                            </select>

                            <input type="submit" value="Сохранить">
                        </form>
                    </c:if>
                    <c:if test="${currentTime ne null}">
                        <span>Выбранo: </span>

                        <span> <c:out value="${timeMap[currentTime]}"/> </span>
                    </c:if>
                </div>

                <div>
                    <p>Усли время не подходит укажите свое</p>
                    <form name="newTimeInput" class="newTimeInput"
                          action="${pageContext.servletContext.contextPath}/admin/seance-configure" method="post">
                        <input name="newTime" type="time">
                        <input type="submit" value="Сохранить">
                    </form>
                </div>
                <div>
                    <c:if test="${currentMovie eq null}">
                        <form name="currentMovieChoice" class="currentMovieChoice"
                              action="${pageContext.servletContext.contextPath}/admin/seance-configure" method="post">

                            <select name="currentMovie">
                                <c:forEach items="${movieDTOs}" var="movie">
                                    <option value="${movie.id}">
                                        <c:out value="${movie.name}"/>
                                    </option>
                                </c:forEach>
                            </select>

                            <input type="submit" value="Сохранить">
                        </form>
                    </c:if>
                    <c:if test="${currentMovie ne null}">
                        <span>Выбранo: </span>

                        <span> <c:out value="${movieMap[currentMovie]}"/> </span>
                    </c:if>
                </div>
                <div>
                    <form name="seanceConfig" action="${pageContext.servletContext.contextPath}/admin/seance-configure"
                          method="post">
                        <input name="movieHouse" hidden value="${movieHouse}">
                        <input name="movieHall" hidden value="${movieHall}">
                        <input name="currentTime" hidden value="${currentTime}">
                        <input name="currentMovie" hidden value="${currentMovie}">
                        <label>
                            <span>День начала показа</span>
                            <input name="dayBegin" type="date" required>
                        </label><br>
                        <label>
                            <span>День окончания показа</span>
                            <input name="dayEnd" type="date" required>
                        </label><br>
                        <label>
                            <span>Цена</span>
                            <input name="price" type="text" required>
                        </label><br>
                        <input type="submit" value="Создать">
                    </form>
                </div>
            </div>
        </div>
    </main>
</div>
</body>
</html>
