'use strict';

(function(){
var modalContent=document.querySelector(".modal-content");
var close=modalContent.querySelector(".modal-content__close");
var mailUs=document.querySelector(".js-mail");
var userName=modalContent.querySelector(".user-name");
var form=modalContent.querySelector("form");
var userEmail= modalContent.querySelector(".user-email");
var userText=modalContent.querySelector(".text");
var storageUserName=localStorage.getItem("userName");
var storageUserEmail=localStorage.getItem("userEmail");

form.addEventListener("submit", function(e){
  if(!userName.value || !userEmail.value || !userText.value){
		e.preventDefault();
		alert("Заполните все поля!");
	}else{
     console.log("submit");
		 localStorage.setItem("userName", userName.value);
		 localStorage.setItem("userEmail", userEmail.value);
	}


});

mailUs.addEventListener("click", function(e){
	e.preventDefault();
	modalContent.classList.remove("modal-content--hide");
 modalContent.classList.add("modal-content--show");

 if(storageUserName && storageUserEmail){
	 userName.value=storageUserName;
	 userEmail.value=storageUserEmail;
 }else{
	 userText.focus();
 }
});

close.addEventListener("click", function(e){
	  e.preventDefault();
		modalContent.classList.add("modal-content--hide");
});


window.addEventListener("keydown", function(e){
  if(e.keyCode===27){
		  if(modalContent.classList.contains("modal-content--hide")){
					modalContent.classList.add("modal-content--hide");	
			}

	}

});

//---------------------------------------------------------------------------
})();//конец скрипта
