package ua.i.pl.s.alex.dao.impl;


import ua.i.pl.s.alex.datasource.DataSource;
import ua.i.pl.s.alex.model.Seat;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 19.03.2017.
 */
public class SeatDaoImpl extends CrudDao<Seat> {
    private final String INSERT = "Insert into seat (seat_row, seat_position, seat_hall," +
            " seat_movie_house, seat_seance) values(?, ?, ?, ?, ?)";

    private final String UPDATE = "Update seat Set seat_row=?, seat_position=? where id=?";
    private final String READ_ALL_BY_HALL_ID = "SELECT * FROM seat where seat_hall=?";
    private final String READ_ALL_BY_SEANCE_ID = "SELECT * FROM seat where seat_seance=?";
    private static SeatDaoImpl seatDao;

    private SeatDaoImpl(Class type) {
        super(type);
    }

    public static SeatDaoImpl getInstance() {
        if (seatDao == null) {
            seatDao = new SeatDaoImpl(Seat.class);
        }
        return seatDao;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Seat entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setInt(1, entity.getSeatRow());
        preparedStatement.setInt(2, entity.getSeatPosition());

        preparedStatement.setInt(3, entity.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Seat entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, entity.getSeatRow());
        preparedStatement.setInt(2, entity.getSeatPosition());

        preparedStatement.setInt(3, entity.getHall());
        preparedStatement.setInt(4, entity.getMovieHouse());
        preparedStatement.setInt(5, entity.getSeance());
        return preparedStatement;
    }

    protected PreparedStatement createInsertBatchStatement(Connection connection, List<Seat> entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        for (Seat seatTmp : entity) {
            preparedStatement.setInt(1, seatTmp.getSeatRow());
            preparedStatement.setInt(2, seatTmp.getSeatPosition());

            preparedStatement.setInt(3, seatTmp.getHall());
            preparedStatement.setInt(4, seatTmp.getMovieHouse());
            preparedStatement.setInt(5, seatTmp.getSeance());
            preparedStatement.addBatch();
        }

        return preparedStatement;
    }
public void saveAll(List<Seat> seatList){
    try(Connection connection=DataSource.getInstance().getConnection();
        PreparedStatement insertBatchStatement= createInsertBatchStatement(connection, seatList);){
         insertBatchStatement.executeBatch();
    } catch (SQLException e) {
        e.printStackTrace();
    }
}

    @Override
    protected List<Seat> readAll(ResultSet resultSet) throws SQLException {
        List<Seat> listSeat = new ArrayList<>();
        Seat seatTmp = null;
        if (!resultSet.next()) {
            System.out.println("В таблице Seance нет записей");
        } else {

            resultSet.beforeFirst();
        }
        //int seatRow, int seatPosition, int hall, int movieHouse
        while (resultSet.next()) {
            seatTmp = new Seat();
            seatTmp.setSeatRow(resultSet.getInt("seat_row"));
            seatTmp.setSeatPosition(resultSet.getInt("seat_position"));
            seatTmp.setHall(resultSet.getInt("seat_hall"));
            seatTmp.setMovieHouse(resultSet.getInt("seat_movie_house"));

            seatTmp.setSeance(resultSet.getInt("seat_seance"));
            seatTmp.setId(resultSet.getInt("id"));
            //  System.out.println(seatTmp);
            listSeat.add(seatTmp);
        }
//        for (Seat seat : listSeat) {
//            System.out.println("listSeat" + seat);
//        }
        return listSeat;
    }


    public List<Seat> readAllByHallId(int key) {
        List<Seat> seatList = null;

        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_BY_HALL_ID)
        ) {
            preparedStatement.setInt(1, key);
            ResultSet resultSet = preparedStatement.executeQuery();
            seatList = readAll(resultSet);
        } catch (SQLException e) {
        }

        return seatList;
    }

    public List<Seat> readAllBySeanceId(int key) {
        List<Seat> seatList = null;

        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_BY_SEANCE_ID)
        ) {
            preparedStatement.setInt(1, key);
            ResultSet resultSet = preparedStatement.executeQuery();
            seatList = readAll(resultSet);
        } catch (SQLException e) {
        }

        return seatList;
    }
}
