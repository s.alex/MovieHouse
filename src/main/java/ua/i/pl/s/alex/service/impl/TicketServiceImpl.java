package ua.i.pl.s.alex.service.impl;

import ua.i.pl.s.alex.dao.DaoFactory;
import ua.i.pl.s.alex.dao.api.Dao;
import ua.i.pl.s.alex.dao.impl.TicketDaoImpl;
import ua.i.pl.s.alex.dto.TicketDTO;
import ua.i.pl.s.alex.mapper.BeanMapper;
import ua.i.pl.s.alex.model.Ticket;
import ua.i.pl.s.alex.service.api.Service;

import java.util.List;

/**
 * Created by Admin on 21.03.2017.
 */
public class TicketServiceImpl implements Service<Integer, TicketDTO> {
    private static TicketServiceImpl service;
    private Dao<Integer, Ticket> ticketDao;
    private BeanMapper beanMapper;

    private TicketDaoImpl seatDaoImpl;

    private TicketServiceImpl() {

        Dao<Integer, Ticket> ticketDao = DaoFactory.getInstance().getTicketDao();
        beanMapper = BeanMapper.getInstance();
        seatDaoImpl = TicketDaoImpl.getInstance();
    }

    public static synchronized TicketServiceImpl getInstance() {
        if (service == null) {
            service = new TicketServiceImpl();
        }

        return service;
    }

    @Override
    public List<TicketDTO> getAll() {
        List<Ticket> ticketList = ticketDao.getAll();
        List<TicketDTO> ticketDTOs = beanMapper.listMapToList(ticketList, TicketDTO.class);
        return ticketDTOs;
    }

    @Override
    public TicketDTO getById(Integer key) {
        Ticket ticket = ticketDao.getById(key);
        TicketDTO ticketDTO = beanMapper.singleMapper(ticket, TicketDTO.class);
        return ticketDTO;
    }

    @Override
    public void save(TicketDTO entity) {
        Ticket ticket = beanMapper.singleMapper(entity, Ticket.class);
        ticketDao.save(ticket);
    }

    public void saveAll(List<TicketDTO> ticketDTOs){
        List<Ticket> ticketList=beanMapper.listMapToList(ticketDTOs, Ticket.class);
        TicketDaoImpl.getInstance().saveAll(ticketList);
    }
    @Override
    public void delete(Integer key) {
        ticketDao.delete(key);
    }

    @Override
    public void update(TicketDTO entity) {
        Ticket ticket = beanMapper.singleMapper(entity, Ticket.class);
        ticketDao.update(ticket);
    }

}
