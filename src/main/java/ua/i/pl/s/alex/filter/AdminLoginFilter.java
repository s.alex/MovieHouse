package ua.i.pl.s.alex.filter;

import ua.i.pl.s.alex.dto.UserDTO;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Admin on 11.04.2017.
 */
@WebFilter(filterName = "AdminLoginFilter", urlPatterns = "/admin/*")
public class AdminLoginFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request=(HttpServletRequest)req;
        HttpServletResponse response=(HttpServletResponse) resp;
        UserDTO userDTO=(UserDTO) request.getSession().getAttribute("userByLogin");
        if(userDTO!=null && userDTO.getPermissions()==1){
        chain.doFilter(req, resp);
        }else{
            request.getSession().setAttribute("url", request.getRequestURI());
            request.getSession().setAttribute("message", "Проверьте правильность введения логина и пароля");
            response.sendRedirect(request.getContextPath()+"/pages/common/login.jsp");
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
