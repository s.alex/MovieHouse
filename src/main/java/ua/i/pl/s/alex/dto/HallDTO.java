package ua.i.pl.s.alex.dto;

import lombok.Getter;
import lombok.Setter;
import ua.i.pl.s.alex.model.Entity;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;


/**
 * Created by Admin on 16.03.2017.
 */
@Getter
@Setter
public class HallDTO extends Entity<Integer> implements Comparable{
    private String name;
    private int seatsQuantity;
    private int rows;
    private int rowSeats;
    private int seance;
    private int movieHouseId;
   // private int[] seats;
//    private int[][] seats;
    private List<Integer> seances;


    public HallDTO(String name, int rows, int rowSeats, int seatsQuantity, int movieHouseId) {
        this.name = name;
        this.seatsQuantity = seatsQuantity;
        this.rows = rows;
        this.rowSeats = rowSeats;
        this.movieHouseId = movieHouseId;
      //  this.seats=new int[ seatsQuantity];
       // this.seats=new int[rows][rowSeats];
//
    }

    public HallDTO() {
    }


    @Override
    public String toString() {
        return "Hall{" +
                "name='" + name + '\'' +
                ", seatsQuantity=" + seatsQuantity +
                ", rows=" + rows +
                ", rowSeats=" + rowSeats +
                ", seance=" + seance +
                ", movieHouseId=" + movieHouseId +
//                ", seats=" + Arrays.toString(seats) +
                ", seances=" + seances +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        int result=-1;
        if(o instanceof HallDTO){
            result=this.getId()-((HallDTO)o).getId();
        }

        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HallDTO)) return false;
        if (!super.equals(o)) return false;
        HallDTO hallDTO = (HallDTO) o;
        return getId() == hallDTO.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getId());
    }
}
