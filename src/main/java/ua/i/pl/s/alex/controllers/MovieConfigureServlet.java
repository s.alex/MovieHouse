package ua.i.pl.s.alex.controllers;


import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import ua.i.pl.s.alex.dto.MovieDTO;
import ua.i.pl.s.alex.helper.FilmByDateGetter;
import ua.i.pl.s.alex.service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by A Sosnovskyi on 12.04.2017.
 */
@MultipartConfig
@WebServlet(name = "MovieConfigureServlet", urlPatterns = "/admin/movie-configure")
public class MovieConfigureServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");
        String title = request.getParameter("title");
        //-----------------------------------------------------------------------------------------------------------------------
        if(title!=null&&title!=""){
            MovieDTO movieDTO = new MovieDTO();
            movieDTO.setName(title);
            movieDTO.setGenre(request.getParameter("genre"));
            movieDTO.setProducedBy(request.getParameter("producedBy"));
            movieDTO.setDuration(Integer.valueOf(request.getParameter("duration")));
            movieDTO.setRating(Integer.valueOf(request.getParameter("rating")));
            movieDTO.setCountry(request.getParameter("country"));
            movieDTO.setActors(request.getParameter("actors"));
            movieDTO.setDescription(request.getParameter("description"));
        Part filePart = request.getPart("file");
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
            movieDTO.setIcon(fileName);
//            System.out.println(fileName);
//            System.out.println(getServletContext().getContextPath() + "/img/icons/" +fileName);
//            System.out.println(getServletContext().getRealPath("\\"));
//            System.out.println("D:\\Documents\\Java_Projects\\MovieHouse\\src\\main\\webapp\\img\\icons");

            List<MovieDTO> allMovies = MovieServiceImpl.getInstance().getAll();
            boolean flag=false;
            for(MovieDTO movie: allMovies){
                if (movie.getName().equals(title)){flag=true;
                break;
                }
            }
           if(!flag){
               MovieServiceImpl.getInstance().save(movieDTO);
               System.out.println("Добавляю");
               //            InputStream fileContent = filePart.getInputStream();
               try(BufferedInputStream bufferedInputStream = new BufferedInputStream(filePart.getInputStream());
                   BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(new File(getServletContext().getRealPath("\\") + "\\img\\icons\\" +fileName)));){
                   int val;
                   while((val=bufferedInputStream.read())!=-1){
                       bufferedOutputStream.write(val);
                   }
                   bufferedOutputStream.flush();
               }catch(FileNotFoundException e){
                   e.printStackTrace();
               }
           }else{
               System.out.println("Такой есть");
               request.setAttribute("message", "Такой есть");
           }

        }

        Object dateObj = request.getSession().getAttribute("date");
        Date date;
        FilmByDateGetter filmHalper = new FilmByDateGetter();
        List<MovieDTO> movieDTOs;
        if (dateObj == null) {
            date = new Date(System.currentTimeMillis());

            movieDTOs = filmHalper.getFilmsByDate(date, date);

        } else {
            date = (Date) dateObj;
            movieDTOs = filmHalper.getFilmsByDate(date, date);
        }

        request.getSession().setAttribute("movieDTOs", movieDTOs);
//==================================================================================================================================
//        if (title != null) {
//            MovieDTO movieDTO = new MovieDTO();
//            movieDTO.setName(title);
//            movieDTO.setGenre(request.getParameter("genre"));
//            movieDTO.setProducedBy(request.getParameter("producedBy"));
//            movieDTO.setDuration(Integer.parseInt(request.getParameter("duration")));
//            movieDTO.setRating(Integer.parseInt(request.getParameter("rating")));
//            movieDTO.setCountry(request.getParameter("country"));
//            movieDTO.setIcon(request.getParameter("icon"));
//            movieDTO.setActors(request.getParameter("actors"));
//            movieDTO.setDescription(request.getParameter("description"));
//
//            String iconFile = request.getParameter("filePath");
//            try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(iconFile));
//                 BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(new File(getServletContext().getContextPath() + "/img/icons/" + iconFile)));
//            ) {
//                int val;
//                while ((val = bufferedInputStream.read()) != -1) {
//                    bufferedOutputStream.write(val);
//                }
//                bufferedOutputStream.flush();
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            MovieServiceImpl.getInstance().save(movieDTO);
//        }
//=================================================================================================================================
        request.getRequestDispatcher("/pages/admin/movie-configure.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    //=================================проблема с кодировкой==================================================
//    protected Map getParams(HttpServletRequest request) {
//        try {
//            request.setCharacterEncoding("utf-8"); // не помагает
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        Map params = new HashMap<>();
//        try {
//            DiskFileUpload diskFileUpload = new DiskFileUpload();
////diskFileUpload.setSizeMax(FILE_MAX_SIZE);
//            System.out.println("diskFileUpload.getHeaderEncoding="+diskFileUpload.getHeaderEncoding());
//            diskFileUpload.setHeaderEncoding("utf-8");
//            List paramsList = diskFileUpload.parseRequest(request);
//            Iterator iterator = paramsList.iterator();
//            while (iterator.hasNext()) {
//                FileItem param = (FileItem) iterator.next();
//                params.put(param.getFieldName(), param);
//            }
//        } catch (FileUploadException e) {
//        }
//        return params;
//    }
    //=================================================================================================
}
