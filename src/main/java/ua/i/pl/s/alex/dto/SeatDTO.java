package ua.i.pl.s.alex.dto;

import lombok.Getter;
import lombok.Setter;
import ua.i.pl.s.alex.model.Entity;

/**
 * Created by Admin on 17.03.2017.
 */
@Getter
@Setter
public class SeatDTO extends Entity<Integer>  {
    private int seatRow; //ряд
    private int seatPosition; //место
    private boolean seatFree; //свободно
    private int hall; // зал
    private int movieHouse;
    private int seance;

    public SeatDTO(int seatRow, int seatPosition, int hall, int movieHouse) {
        this.seatRow = seatRow;
        this.seatPosition = seatPosition;
        this.hall = hall;
        this.movieHouse = movieHouse;
        this.seatFree=true;
    }

    public SeatDTO() {
    }

    @Override
    public String toString() {
        return "Seat{" +
                "seatRow=" + seatRow +
                ", seatPosition=" + seatPosition +

                ", hall=" + hall +
                ", movieHouse=" + movieHouse +
                ", seance=" + seance +
                '}';
    }
}
