package ua.i.pl.s.alex.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;


/**
 * Created by Admin on 16.03.2017.
 */
@Getter
@Setter
public class Hall extends Entity<Integer> implements Comparable{
    private String name;
    private int seatsQuantity;
    private int rows;
    private int rowSeats;
    private int seance;
    private int movieHouseId;
   // private int[] seats;
 //  private int[][] seats;
    private List<Integer> seances;


    public Hall(String name, int rows, int rowSeats, int seatsQuantity, int movieHouseId) {
        this.name = name;
        this.seatsQuantity = seatsQuantity;
        this.rows = rows;
        this.rowSeats = rowSeats;
        this.movieHouseId = movieHouseId;
       // this.seats=new int[ seatsQuantity];
        //this.seats=new int[ rows][rowSeats];

    }

    public Hall() {
    }


    @Override
    public String toString() {
        return "Hall{" +
                "name='" + name + '\'' +
                ", seatsQuantity=" + seatsQuantity +
                ", rows=" + rows +
                ", rowSeats=" + rowSeats +
                ", seance=" + seance +
                ", movieHouseId=" + movieHouseId +
//                ", seats=" + Arrays.toString(seats) +
                ", seances=" + seances +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        int result=-1;
        if(o instanceof Hall){
            result=this.getId()-((Hall)o).getId();
        }

        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hall)) return false;
        if (!super.equals(o)) return false;
        Hall hall = (Hall) o;
        return getId() == hall.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getId());
    }
}
