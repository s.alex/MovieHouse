package ua.i.pl.s.alex.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import ua.i.pl.s.alex.model.Entity;

import java.util.List;

/**
 * Created by A Sosnovsky  on 07.02.2017.
 */
@Getter
@Setter
@EqualsAndHashCode
public class UserDTO extends Entity<Integer> {

    private String userName; //Имя
    private String userSurname; //Фамилия
    private String userLogin; //Login
    private String userPassword; // Password
    private String userEmail; // Email
    private int userDiscount;
    private List<Integer> purchasedTickets; //купленный билет
    private int permissions;  //права доступа

    public UserDTO(String usernName, String userSurname, String userLogin, String userPassword, String userEmail) {

        this.userName = usernName;
        this.userSurname = userSurname;
        this.userLogin = userLogin;
        this.userPassword = userPassword;
        this.userEmail = userEmail;
        this.permissions = 0;
    }


    public UserDTO() {
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "userId=" + getId() +
                ", userName='" + userName + '\'' +
                ", userSurname='" + userSurname + '\'' +
                ", userLogin='" + userLogin + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", userDiscount=" + userDiscount +
                ", purchasedTickets=" + purchasedTickets +
                ", permissions=" + permissions +
                '}';
    }
}


