package ua.i.pl.s.alex.service.impl;

import ua.i.pl.s.alex.dao.DaoFactory;
import ua.i.pl.s.alex.dao.api.Dao;
import ua.i.pl.s.alex.dto.MovieDTO;
import ua.i.pl.s.alex.mapper.BeanMapper;
import ua.i.pl.s.alex.model.Movie;
import ua.i.pl.s.alex.service.api.Service;

import java.util.List;

/**
 * Created by Admin on 01.04.2017.
 */
public class MovieServiceImpl implements Service<Integer, MovieDTO> {
    private static MovieServiceImpl service;
    private Dao<Integer, Movie> movieDao;
    private BeanMapper beanMapper;

    private MovieServiceImpl() {
        movieDao= DaoFactory.getInstance().getMovieDao();
        beanMapper=BeanMapper.getInstance();
    }
    public static synchronized MovieServiceImpl getInstance(){
        if(service==null){
            service=new MovieServiceImpl();
        }

        return service;
    }

    @Override
    public List<MovieDTO> getAll() {
        List<Movie> movies = movieDao.getAll();
        List<MovieDTO> movieDTOs = beanMapper.listMapToList(movies, MovieDTO.class);
//        for (MovieDTO movieDTO: movieDTOs){
//            System.out.println(movieDTO);
//        }
        return movieDTOs;
    }

    @Override
    public MovieDTO getById(Integer key) {
        Movie movie= movieDao.getById(key);
        MovieDTO movieDTO=BeanMapper.singleMapper(movie, MovieDTO.class);
        return movieDTO;
    }

    @Override
    public void save(MovieDTO movieDto) {
        Movie movie = beanMapper.singleMapper(movieDto, Movie.class);
        movieDao.save(movie);
    }

    @Override
    public void delete(Integer key) {
        movieDao.delete(key);
    }

    @Override
    public void update(MovieDTO entity) {
        Movie movie = beanMapper.singleMapper(entity, Movie.class);
        movieDao.update(movie);
    }
}
