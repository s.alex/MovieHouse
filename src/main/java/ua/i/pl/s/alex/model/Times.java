package ua.i.pl.s.alex.model;

import lombok.Getter;
import lombok.Setter;

import java.sql.Time;
import java.util.Objects;

/**
 * Created by Admin on 06.04.2017.
 */
@Getter
@Setter
public class Times implements Comparable {
    private int id;
    private Time time;

    public Times() {
    }

    @Override
    public int compareTo(Object o) {
        return this.getId()-((Times)o).getId();

    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Times)) return false;
        Times times = (Times) o;
        return
                Objects.equals(getTime(), times.getTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash( getTime());
    }
}
