package ua.i.pl.s.alex.controllers;

import ua.i.pl.s.alex.dto.UserDTO;
import ua.i.pl.s.alex.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 11.04.2017.
 */
@WebServlet(name = "AdminConfigurationServlet", urlPatterns = "/admin/admin-configure")
public class AdminConfigurationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");
        String userToDeleteString = request.getParameter("user");
        String userToAddString = request.getParameter("surname");

        if (userToAddString != null) {
            UserDTO userDTOtoAdd = new UserDTO();
            userDTOtoAdd.setUserSurname(userToAddString);
            userDTOtoAdd.setUserName(request.getParameter("name"));
            userDTOtoAdd.setUserLogin(request.getParameter("login"));

            userDTOtoAdd.setUserPassword(request.getParameter("password"));
            userDTOtoAdd.setUserEmail(request.getParameter("email"));
            String permissionString = request.getParameter("permission");
            int permission;
            if (permissionString.equals("on")) {
                permission = 1;
            } else {
                permission = 0;
            }
            userDTOtoAdd.setPermissions(permission);
            UserServiceImpl.getInstance().save(userDTOtoAdd);
        }
        if (userToDeleteString != null) {
            int key = Integer.parseInt(userToDeleteString);
            UserServiceImpl.getInstance().delete(key);
        }
        List<UserDTO> userDTOList = UserServiceImpl.getInstance().getAllByPermission(1);
        request.setAttribute("userDTOList", userDTOList);
        request.getRequestDispatcher("/pages/admin/admin-configure.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
