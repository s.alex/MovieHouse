package ua.i.pl.s.alex.service.impl;


import ua.i.pl.s.alex.dao.DaoFactory;
import ua.i.pl.s.alex.dao.api.Dao;
import ua.i.pl.s.alex.dao.impl.HallDaoImpl;
import ua.i.pl.s.alex.dao.impl.SeanceDaoImpl;
import ua.i.pl.s.alex.dao.impl.SeatDaoImpl;
import ua.i.pl.s.alex.dto.HallDTO;
import ua.i.pl.s.alex.mapper.BeanMapper;
import ua.i.pl.s.alex.model.Hall;
import ua.i.pl.s.alex.model.Seance;
import ua.i.pl.s.alex.model.Seat;
import ua.i.pl.s.alex.service.api.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 21.03.2017.
 */
public class HallServiceImpl implements Service<Integer, HallDTO> {
    private static HallServiceImpl service;
    private Dao<Integer, Hall> hallDao;
    private HallDaoImpl hallHallDao;
    private BeanMapper beanMapper;

    private HallServiceImpl() {
        hallDao = DaoFactory.getInstance().getHallDao();
        beanMapper = BeanMapper.getInstance();
        hallHallDao = HallDaoImpl.getInstance();
    }

    public static synchronized HallServiceImpl getInstance() {
        if (service == null) {
            service = new HallServiceImpl();
        }

        return service;
    }


    @Override
    public List<HallDTO> getAll() {
        List<Hall> halls = hallDao.getAll();
//        for (Hall hall : halls) {
//            List<Seat> seatList = SeatDaoImpl.getInstance().readAllByHallId(hall.getId());
//            int i=0;
//            int[] seats=new int[hall.getSeatsQuantity()];
//
//
//            for(Seat seat:seatList){
//
//                seats[i]=seat.getId();
//                i++;
//            }
//          //  hall.setSeats(1);
//        }
        List<HallDTO> hallsDTO = beanMapper.listMapToList(halls, HallDTO.class);
        return hallsDTO;
    }

    @Override
    public HallDTO getById(Integer key) {
        Hall hall = hallDao.getById(key);
        HallDTO hallDTO = BeanMapper.singleMapper(hall, HallDTO.class);
        return hallDTO;
    }

    @Override
    public void save(HallDTO entity) {
        Hall hall = beanMapper.singleMapper(entity, Hall.class);
        hallDao.save(hall);
    }

    @Override
    public void delete(Integer key) {
        hallDao.delete(key);
    }

    @Override
    public void update(HallDTO entity) {
        Hall hall = beanMapper.singleMapper(entity, Hall.class);
        hallDao.update(hall);
    }

    public List<HallDTO> getAllByMovieHouseId(int key) {

        List<Hall> halls = hallHallDao.readAllByMovieHouseId(key);
//        SeatDaoImpl setDao = SeatDaoImpl.getInstance();
//        SeanceDaoImpl seanceDao = SeanceDaoImpl.getInstance();

//        for (Hall hall : halls) {
//            List<Seat> seatList = setDao.readAllByHallId(hall.getId());
//            List<Seance> seanceList = seanceDao.readAllByHallId(hall.getId());
//            int i = 0;
//            int[] seats = new int[hall.getSeatsQuantity()];
//            for (Seat seat : seatList) {
//                seats[i] = seat.getId();
//                i++;
//            }
//            List<Integer> listSeanceID=new ArrayList<>();
//             for(Seance seance:seanceList){
//                 listSeanceID.add(seance.getId());
//             }
//            hall.setSeats(seats);
//            hall.setSeances(listSeanceID);
//        }

        //-----------------------------------------------------------------------------------------
         List<HallDTO> hallsDTO = beanMapper.listMapToList(halls, HallDTO.class);
        return hallsDTO;
    }
}
