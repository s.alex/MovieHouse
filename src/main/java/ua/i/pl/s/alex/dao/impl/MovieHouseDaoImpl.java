package ua.i.pl.s.alex.dao.impl;

import ua.i.pl.s.alex.datasource.DataSource;
import ua.i.pl.s.alex.model.MovieHouse;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Admin on 18.03.2017.
 */
public class MovieHouseDaoImpl extends CrudDao<MovieHouse> {
    private final String INSERT = "Insert into moviehouse (moviehouse_name, moviehouse_halls) values(?, ?)";
    private final String UPDATE = "Update moviehouse Set moviehouse_name=?, moviehouse_halls=? where id=?";
    private final String FIND_BY_NAME = "Select * from moviehouse where moviehouse_name =?";
    private static MovieHouseDaoImpl crudDAO;

    private MovieHouseDaoImpl(Class type) {
        super(type);
    }

    public static MovieHouseDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new MovieHouseDaoImpl(MovieHouse.class);
        }
        return crudDAO;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, MovieHouse entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setInt(2, entity.getHallsQuantity());
        preparedStatement.setInt(3, entity.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, MovieHouse entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setInt(2, entity.getHallsQuantity());
        return preparedStatement;
    }

    @Override
    protected List<MovieHouse> readAll(ResultSet resultSet) throws SQLException {
        List<MovieHouse> movieHouseList = new LinkedList<>();
        MovieHouse movieHouse = null;
//        if (!resultSet.next()) {
//            System.out.println("В таблице MovieHouse нет записей");
//        } else {
//            resultSet.beforeFirst();
//        }
        while (resultSet.next()) {
            movieHouse = new MovieHouse();
            movieHouse.setId(resultSet.getInt("id"));
            movieHouse.setName(resultSet.getString("moviehouse_name"));
            movieHouse.setHallsQuantity(resultSet.getInt("moviehouse_halls"));
            movieHouseList.add(movieHouse);
        }
        return movieHouseList;
    }
//===============================================================================================
    public MovieHouse getByName( String name)  {
        List result=null;
        try (Connection connection= DataSource.getInstance().getConnection();
             PreparedStatement  preparedStatement = connection.prepareStatement(FIND_BY_NAME);){
            preparedStatement.setString(1, name);
            ResultSet resultSet=preparedStatement.executeQuery();
            result=readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (MovieHouse) result.get(0);
    }
    //=========================================================================================
}
