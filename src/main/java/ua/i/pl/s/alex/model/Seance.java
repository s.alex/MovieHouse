package ua.i.pl.s.alex.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by Admin on 17.03.2017.
 */
@Getter
@Setter
public class Seance extends Entity<Integer> {
    private Date dayBegin;
    private Date dayEnd;
    private int currentSeanceTime;
  //  private List<Date> timesOfShowTable;
    private List<Integer> seats;
    private int movieId;
    private int hallId;
    private int movieHouseId;
    private int price;

    public Seance(Date dayBegin, Date dayEnd , int currentSeanceTime, int movieId, int hallId, int movieHouseId, int price) {
        this.dayBegin = dayBegin;
        this.dayEnd = dayEnd;
        this.currentSeanceTime=currentSeanceTime;
        this.movieId = movieId;
        this.hallId = hallId;
        this.movieHouseId=movieHouseId;
        this.price = price;
    }

    public Seance() {
    }

    @Override
    public String toString() {
        return "Seance{" +
                "id="+getId()+
                ", dayBegin=" + dayBegin +
                ", dayEnd=" + dayEnd +
                ", currentSeanceTime=" + currentSeanceTime +
                ", movieId=" + movieId +
                ", hallId=" + hallId +
                ", movieHouseId=" + movieHouseId +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Seance)) return false;
        if (!super.equals(o)) return false;
        Seance seance = (Seance) o;
        return getCurrentSeanceTime() == seance.getCurrentSeanceTime() &&
                getMovieId() == seance.getMovieId() &&
                getHallId() == seance.getHallId() &&
                getMovieHouseId() == seance.getMovieHouseId()  &&
                Objects.equals(getDayBegin(), seance.getDayBegin()) &&
                Objects.equals(getDayEnd(), seance.getDayEnd());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getDayBegin(), getDayEnd(), getCurrentSeanceTime(), getMovieId(), getHallId(), getMovieHouseId(), getPrice());
    }
}
