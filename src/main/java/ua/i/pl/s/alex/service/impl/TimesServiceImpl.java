package ua.i.pl.s.alex.service.impl;

import ua.i.pl.s.alex.dao.impl.TimeDaoImpl;
import ua.i.pl.s.alex.dto.TimesDTO;
import ua.i.pl.s.alex.mapper.BeanMapper;
import ua.i.pl.s.alex.model.Times;
import java.util.List;

/**
 * Created by Admin on 07.04.2017.
 */
public class TimesServiceImpl {
    private static TimesServiceImpl service;
    private TimeDaoImpl timesDao;
    private BeanMapper beanMapper;

    private TimesServiceImpl() {
        timesDao =  TimeDaoImpl.getInstance();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized TimesServiceImpl getInstance() {
        if (service == null) {
            service = new TimesServiceImpl();
        }

        return service;
    }
    public TimesDTO getById(Integer key) {
        Times times = timesDao.readById(key);
        TimesDTO timesDTO = beanMapper.singleMapper(times, TimesDTO.class);
        return timesDTO;
    }
    public List<TimesDTO> getAll() {
        List<Times> times = TimeDaoImpl.getInstance().getAll();
        List<TimesDTO> timesDTO = beanMapper.listMapToList(times, TimesDTO.class);
        return timesDTO;
    }

    public void save(TimesDTO entity){
        Times time=beanMapper.singleMapper(entity, Times.class);
        TimeDaoImpl.getInstance().save(time);
    }
}
