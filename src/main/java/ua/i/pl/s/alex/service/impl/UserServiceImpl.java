package ua.i.pl.s.alex.service.impl;



import ua.i.pl.s.alex.dao.DaoFactory;
import ua.i.pl.s.alex.dao.api.Dao;
import ua.i.pl.s.alex.dao.impl.UserDaoImpl;
import ua.i.pl.s.alex.dto.UserDTO;
import ua.i.pl.s.alex.mapper.BeanMapper;
import ua.i.pl.s.alex.model.User;
import ua.i.pl.s.alex.service.api.Service;

import java.util.List;

/**
 * Created by Admin on 21.03.2017.
 */
public class UserServiceImpl implements Service<Integer, UserDTO> {
    private static UserServiceImpl service;

    private Dao<Integer, User> userDao;
    private BeanMapper beanMapper;

    private UserServiceImpl() {
        userDao = DaoFactory.getInstance().getUserDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized UserServiceImpl getInstance() {
        if (service == null) {
            service = new UserServiceImpl();
        }

        return service;
    }

    @Override
    public List<UserDTO> getAll() {
        List<User> userList = userDao.getAll();
        List<UserDTO> userDTOList = beanMapper.listMapToList(userList, UserDTO.class);
        return userDTOList;
    }

    @Override
    public UserDTO getById(Integer key) {
        User user = userDao.getById(key);
        UserDTO userDTO = beanMapper.singleMapper(user, UserDTO.class);
        return userDTO;
    }

    @Override
    public void save(UserDTO entity) {
        User user = beanMapper.singleMapper(entity, User.class);
        userDao.save(user);
    }

    @Override
    public void delete(Integer key) {
        userDao.delete(key);

    }

    @Override
    public void update(UserDTO entity) {
        User user = beanMapper.singleMapper(entity, User.class);
        userDao.update(user);
    }

    public UserDTO getByLogin(String login){
        User user = UserDaoImpl.getInstance().readByLogin(login);
        UserDTO userDTO=beanMapper.singleMapper(user, UserDTO.class);
        return userDTO;
    }
    public List<UserDTO> getAllByPermission(int permission){
        List<User> userList=UserDaoImpl.getInstance().readAllByPermission(permission);
        List<UserDTO> userDTOList=beanMapper.listMapToList(userList, UserDTO.class);
        return userDTOList;
    }
}
