package ua.i.pl.s.alex.dto;


import lombok.Getter;
import lombok.Setter;

import java.sql.Time;
import java.util.Objects;

/**
 * Created by Admin on 06.04.2017.
 */
@Getter
@Setter
public class TimesDTO implements Comparable{
    @Override
    public int compareTo(Object o) {
        return this.getId()-((TimesDTO)o).getId();

    }

    private int id;
    private Time time;

    public TimesDTO() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TimesDTO)) return false;
        TimesDTO timesDTO = (TimesDTO) o;
        return
                Objects.equals(getTime(), timesDTO.getTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash( getTime());
    }
}
