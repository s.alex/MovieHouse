package ua.i.pl.s.alex.controllers;

import ua.i.pl.s.alex.dto.MovieHouseDTO;
import ua.i.pl.s.alex.dto.SeanceDTO;
import ua.i.pl.s.alex.service.impl.HallServiceImpl;
import ua.i.pl.s.alex.service.impl.MovieHouseServiceImpl;
import ua.i.pl.s.alex.service.impl.SeanceServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Admin on 03.04.2017.
 */
@WebServlet(name = "MovieHousesServlet", urlPatterns = "/movie-houses")
public class MovieHousesServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String row_id = request.getParameter("movie_id");

        SeanceServiceImpl seanceInstance = SeanceServiceImpl.getInstance();
        MovieHouseServiceImpl movieHouseInstance = MovieHouseServiceImpl.getInstance();
        List<SeanceDTO> seanceList = null;
        List<MovieHouseDTO> movieHouseAll = new ArrayList<>();

        int movie_id = 0;
        if (row_id != null) {
            movie_id = Integer.parseInt(row_id);
        } else {
            movie_id = 0;
        }

        if (movie_id == 0) {
            movieHouseAll = movieHouseInstance.getAll();
            request.setAttribute("movieHouseAll", movieHouseAll);
        } else {
            seanceList = seanceInstance.getAllByMovieId(movie_id);
            Set<MovieHouseDTO> movieHouseDTOs = new HashSet<>();
            for (SeanceDTO entity : seanceList) {
                MovieHouseDTO movieHouseDTO = movieHouseInstance.getById(entity.getMovieHouseId());

                movieHouseDTOs.add(movieHouseDTO);
            }
            request.getSession().setAttribute("movie_id", movie_id);
            request.setAttribute("movieHouseAll", movieHouseDTOs);
        }


        request.getRequestDispatcher("/pages/common/movie-houses.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
