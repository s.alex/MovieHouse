package ua.i.pl.s.alex.controllers;

import ua.i.pl.s.alex.dto.*;
import ua.i.pl.s.alex.model.Ticket;
import ua.i.pl.s.alex.service.impl.SeanceServiceImpl;
import ua.i.pl.s.alex.service.impl.SeatServiceImpl;
import ua.i.pl.s.alex.service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 09.04.2017.
 */
@WebServlet(name = "SellServlet", urlPatterns = "/sell")
public class SellServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String success = request.getParameter("success");
        int done=0;
        if (success != null) {
//            System.out.println(success);
            Object ticketList = request.getSession().getAttribute("ticketList");
            List<TicketDTO> tickets = (List<TicketDTO>) ticketList;
            UserDTO user = (UserDTO) request.getSession().getAttribute("user");
            SeanceDTO currentSeance = (SeanceDTO) request.getSession().getAttribute("currentSeance");
            HallDTO currentHall = (HallDTO) request.getSession().getAttribute("currentHall");
            MovieHouseDTO currentMovieHouse = (MovieHouseDTO) request.getSession().getAttribute("currentMovieHouse");
            Time time = new Time(System.nanoTime());
            TicketServiceImpl ticketService = TicketServiceImpl.getInstance();
            SeatServiceImpl seatService = SeatServiceImpl.getInstance();
            for (TicketDTO ticket : tickets) {
                ticket.setPrice(currentSeance.getPrice());
                ticket.setHall(currentSeance.getHallId());
                ticket.setMovieHouseName(currentSeance.getMovieHouseId());
                ticket.setTimeOfSell(time);
                done=1;
            }
            ticketService.saveAll(tickets);
            List<SeatDTO> seatDTOList=new ArrayList<>();
            for(TicketDTO ticketDTO: tickets){
//                System.out.println(ticketDTO);
                SeatDTO seatDTO=new SeatDTO();
                seatDTO.setHall(ticketDTO.getHall());
                seatDTO.setMovieHouse(ticketDTO.getMovieHouseName());
                seatDTO.setSeance(ticketDTO.getSeanceId());
                seatDTO.setSeatRow(ticketDTO.getSeatRow());
                seatDTO.setSeatPosition(ticketDTO.getSeatPosition());
                seatDTOList.add(seatDTO);
            }
            seatService.saveAll(seatDTOList);
        }

        request.setAttribute("done", done);
        request.getRequestDispatcher("/pages/common/sell.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
