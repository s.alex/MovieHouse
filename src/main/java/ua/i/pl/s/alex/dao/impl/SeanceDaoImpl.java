package ua.i.pl.s.alex.dao.impl;



import ua.i.pl.s.alex.datasource.DataSource;
import ua.i.pl.s.alex.model.Seance;

import java.sql.*;
import java.sql.Date;
import java.util.*;

/**
 * Created by Admin on 20.03.2017.
 */
public class SeanceDaoImpl extends CrudDao<Seance> {
    private final String INSERT = "Insert into seance (seance_date_begin, seance_date_end, seance_time, seance_movie," +
            " seance_hall, seance_sinema, seance_price) values(?, ?, ?, ?, ?, ?, ?)";
    private final String UPDATE = "Update seance Set seance_day_begin=?, seance_day_end=?, seance_time=?, seance_movie=?," +
            " seance_hall=?, seance_sinema=?, seance_price=? where id=?";
   //SELECT * from seance WHERE seance_hall='1' and seance_date_begin<='2017-04-27' and seance_date_end>'2017-04-27'
    private final  String READ_ALL_BY_MOVIE_HALL_ID="SELECT * from seance WHERE seance_hall=? and seance_date_begin<=? and seance_date_end>?";
    private final  String READ_ALL_BY_MOVIE_DATE="SELECT * from seance WHERE seance_date_begin<=? and seance_date_end>?";
    private final  String READ_ALL_BY_MOVIE_ID="SELECT * from seance WHERE seance_movie=?";
    private final String READ_ALL_BY_MOVIE_HOUSE_ID="SELECT * from seance WHERE seance_sinema=?";
    private final String READ_ALL_BY_MOVIE_HOUSE_ID_AND_DATE="SELECT * from seance WHERE  seance_sinema=? and seance_date_begin<=? and seance_date_end>?";
    private static SeanceDaoImpl seanceDao;

    private SeanceDaoImpl(Class type) {
        super(type);
    }

    public static SeanceDaoImpl getInstance() {
        if (seanceDao == null) {
            seanceDao = new SeanceDaoImpl(Seance.class);
        }
        return seanceDao;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Seance entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Seance entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
//        System.out.println(new Date(entity.getDayBegin().getTime()));
//        System.out.println(new Date(entity.getDayEnd().getTime()));
        preparedStatement.setDate(1, new Date(entity.getDayBegin().getTime()) );
        preparedStatement.setDate(2, new Date(entity.getDayEnd().getTime()) );
        preparedStatement.setInt(3, entity.getCurrentSeanceTime());
        preparedStatement.setInt(4, entity.getMovieId());
        preparedStatement.setInt(5, entity.getHallId());
        preparedStatement.setInt(6, entity.getMovieHouseId());
        preparedStatement.setInt(7, entity.getPrice());
        return preparedStatement;
    }

    @Override
    protected List<Seance> readAll(ResultSet resultSet) throws SQLException {

        List<Seance> seanceList=new ArrayList<>();
        Seance seanceTmp=null;
        if(!resultSet.next()){
            System.out.println("В таблице Seance нет записей");
        }else{

            resultSet.beforeFirst();
        }
        //Date dayBegin, Date dayEnd , seance_time,  int movieId, int hallId, int movieHouseId, int price
        while (resultSet.next()){


            seanceTmp=new Seance(resultSet.getDate("seance_date_begin"), resultSet.getDate("seance_date_end"),
                    resultSet.getInt("seance_time"), resultSet.getInt("seance_movie"), resultSet.getInt("seance_hall"),
                    resultSet.getInt("seance_sinema"), resultSet.getInt("seance_price"));
            seanceTmp.setId(resultSet.getInt("id"));

            seanceList.add(seanceTmp);

        }

        return seanceList;
    }
//    public List<Seance> readAllByHallId(int key){
//       Date date= new Date(System.currentTimeMillis());
//
//        List<Seance> seanceList=null;
//        try (Connection connection = DataSource.getInstance().getConnection();
//             PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_BY_MOVIE_HALL_ID);
//        ) {
//            preparedStatement.setInt(1, key);
//            preparedStatement.setDate(2, date);
//            preparedStatement.setDate(3, date);
//            ResultSet resultSet = preparedStatement.executeQuery();
//
//            seanceList = readAll(resultSet);
//        } catch (SQLException e) {
//        }
//
//        return seanceList;
//    }

    public List<Seance> readAllByDate(Date date1, Date date2){


        List<Seance> seanceList=null;
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_BY_MOVIE_DATE);
        ) {

            preparedStatement.setDate(1, date1);
            preparedStatement.setDate(2, date2);
            ResultSet resultSet = preparedStatement.executeQuery();

            seanceList = readAll(resultSet);
        } catch (SQLException e) {
        }

        return seanceList;
    }


    public List<Seance> readAllByMovieId(int key){

        List<Seance> seanceList=null;
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_BY_MOVIE_ID);
        ) {

            preparedStatement.setInt(1, key);

            ResultSet resultSet = preparedStatement.executeQuery();

            seanceList = readAll(resultSet);
        } catch (SQLException e) {
        }

        return seanceList;
    }
//    public List<Seance> readAllByMovieHouseId(int key){
//
//
//        List<Seance> seanceList=null;
//        try (Connection connection = DataSource.getInstance().getConnection();
//             PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_BY_MOVIE_HOUSE_ID);
//        ) {
//
//            preparedStatement.setInt(1, key);
//
//            ResultSet resultSet = preparedStatement.executeQuery();
//
//            seanceList = readAll(resultSet);
//        } catch (SQLException e) {
//        }
//
//        return seanceList;
//    }

    public List<Seance> readAllByMovieHouseIdAndDate(int key, Date date1, Date date2){


        List<Seance> seanceList=null;
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_BY_MOVIE_HOUSE_ID_AND_DATE);
        ) {

            preparedStatement.setInt(1, key);
            preparedStatement.setDate(2, date1);
            preparedStatement.setDate(3, date2);
            ResultSet resultSet = preparedStatement.executeQuery();

            seanceList = readAll(resultSet);
        } catch (SQLException e) {
        }

        return seanceList;
    }
}
