package ua.i.pl.s.alex.dao.impl;

import ua.i.pl.s.alex.datasource.DataSource;
import ua.i.pl.s.alex.model.Times;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 06.04.2017.
 */
public class TimeDaoImpl {
    private final String READ_All = "SELECT * FROM times";
    private final String READ_BY_ID = "SELECT * FROM times WHERE id=?";
    private final String INSERT="INSERT into times(times_time) values(?)";
    private static TimeDaoImpl timeDao;

    public static TimeDaoImpl getInstance() {
        if (timeDao == null) {
            timeDao = new TimeDaoImpl();
        }
        return timeDao;
    }

    protected List<Times> readAll(ResultSet resultSet) throws SQLException {


        List<Times> timeList = new ArrayList<>();
        Times timeTmp = null;

        while (resultSet.next()) {
            timeTmp = new Times();

            timeTmp.setTime(resultSet.getTime("times_time"));
            timeTmp.setId(resultSet.getInt("id"));
            timeList.add(timeTmp);

        }

        return timeList;
    }
public List<Times> getAll(){
    List<Times> times=null;
    try (Connection connection=DataSource.getInstance().getConnection();
         PreparedStatement preparedStatement=connection.prepareStatement("SELECT * FROM times")){
        ResultSet resultSet = preparedStatement.executeQuery();
        times=readAll(resultSet);
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return times;
}

    public Times readById(int key) {

        List<Times> timeList = null;
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(READ_BY_ID);
        ) {

            preparedStatement.setInt(1, key);

            ResultSet resultSet = preparedStatement.executeQuery();

            timeList = readAll(resultSet);
        } catch (SQLException e) {
        }

        return timeList.get(0);
    }

    public void save(Times entity){
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        ) {

            preparedStatement.setTime(1, new Time(entity.getTime().getTime()));

            int i = preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if(rs.next()){
                entity.setId(rs.getInt(1));

            }
        } catch (SQLException e) {
        }
    }
}
