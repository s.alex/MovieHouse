package ua.i.pl.s.alex.controllers;

import ua.i.pl.s.alex.dto.UserDTO;
import ua.i.pl.s.alex.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Admin on 11.04.2017.
 */
@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        UserDTO userByLogin = UserServiceImpl.getInstance().getByLogin(login);
        if(userByLogin!=null&&userByLogin.getUserPassword().equals(password)&&userByLogin.getPermissions()==1){
            request.getSession().setAttribute("userByLogin", userByLogin);
            if(request.getSession().getAttribute("url")!=null){
                response.sendRedirect(request.getSession().getAttribute("url").toString());
            }else{response.sendRedirect(request.getContextPath()+"/pages/admin/index.jsp");}
        }else{
          request.getSession().setAttribute("message", "Wrong user name or password");
            response.sendRedirect(request.getContextPath()+"/pages/common/login.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
doPost(request, response);
    }
}
