package ua.i.pl.s.alex.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created by Admin on 16.03.2017.
 */
@Getter
@Setter
public class MovieHouse extends Entity<Integer> implements Comparable{
    private String name;
    private int hallsQuantity;
    private int[] movieHalls;


    public MovieHouse() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MovieHouse)) return false;
//        if (!super.equals(o)) return false;
        MovieHouse that = (MovieHouse) o;
        return getHallsQuantity() == that.getHallsQuantity() &&
                Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash( 14*getId()+ getHallsQuantity());
    }

    public MovieHouse(String name, int hallsQuantity) {
        this.name = name;
        this.hallsQuantity = hallsQuantity;
        this.movieHalls = new int[hallsQuantity];
    }

public void setMovieHalls(int[] halls){
    this.movieHalls=halls;
}
    @Override
    public String toString() {
        return "MovieHouse{" +
                "movieHouseId="+this.getId()+
                " name='" + name + '\'' +
                ", halls=" + Arrays.toString(movieHalls) +
                ", hallsQuantity=" + hallsQuantity +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        return this.getId()-((MovieHouse)o).getId();
    }
}

