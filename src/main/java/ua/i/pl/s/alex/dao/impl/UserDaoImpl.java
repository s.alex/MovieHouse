package ua.i.pl.s.alex.dao.impl;



import ua.i.pl.s.alex.datasource.DataSource;
import ua.i.pl.s.alex.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 20.03.2017.
 */
public class UserDaoImpl extends CrudDao<User> {
    private final  String INSERT="Insert into users (user_name, user_surname, user_login, user_password, " +
            "user_email, user_discount,  permissions) values(?, ?, ?, ?, ?, ?, ?)";
    private final  String UPDATE="Update users Set user_name=?, user_surname=?, user_login=?, user_password=?," +
            "user_email=?, user_discount=?,  permissions=? where id=?";
    private final  String READ_ALL_BY_LOGIN="SELECT* FROM users WHERE user_login=?";
    private final String READ_ALL_BY_PERMISSION="SELECT* FROM users WHERE permissions=?";
    private static UserDaoImpl userDao;

    private UserDaoImpl(Class type) {
        super(type);
    }

    public static UserDaoImpl getInstance() {
        if (userDao == null) {
            userDao = new UserDaoImpl(User.class);
        }
        return userDao;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement preparedStatement=connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, entity.getUserName());
        preparedStatement.setString(2, entity.getUserSurname());
        preparedStatement.setString(3, entity.getUserLogin());
        preparedStatement.setString(4, entity.getUserPassword());
        preparedStatement.setString(5, entity.getUserEmail());
        preparedStatement.setInt(6, entity.getUserDiscount());
        preparedStatement.setInt(7, entity.getPermissions());
        preparedStatement.setInt(8, entity.getId());
        return preparedStatement;
    }
  //  user_name, user_surname, user_login, user_password, user_email, user_discount,  permissions
    @Override
    protected PreparedStatement createInsertStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement preparedStatement=connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getUserName());
        preparedStatement.setString(2, entity.getUserSurname());
        preparedStatement.setString(3, entity.getUserLogin());
        preparedStatement.setString(4, entity.getUserPassword());
        preparedStatement.setString(5, entity.getUserEmail());
        preparedStatement.setInt(6, entity.getUserDiscount());
        preparedStatement.setInt(7, entity.getPermissions());

        return preparedStatement;
    }

    @Override
    protected List<User> readAll(ResultSet resultSet) throws SQLException {
        List<User> userList=new ArrayList<>();
        User user=null;
        while (resultSet.next()) {
            user = new User();
            user.setUserName(resultSet.getString("user_name"));
            user.setUserSurname(resultSet.getString("user_surname"));
            user.setUserLogin(resultSet.getString("user_login"));
            user.setUserPassword(resultSet.getString("user_password"));

            user.setUserEmail(resultSet.getString("user_email"));
            user.setUserDiscount(resultSet.getInt("user_discount"));
            user.setPermissions(resultSet.getInt("permissions"));
            user.setId(resultSet.getInt("id"));
            //  System.out.println(seatTmp);
            userList.add(user);
        }
        return userList;
    }

    public List<User> readAllByPermission(int permission){
        List<User> userList=null;
        try (Connection connection=DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement=connection.prepareStatement(READ_ALL_BY_PERMISSION)){
            preparedStatement.setInt(1, permission);
            ResultSet resultSet = preparedStatement.executeQuery();
            userList=readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userList;
    }

    public User readByLogin(String login){
        List<User> users=null;
        User userTmp=null;
        try(Connection connection= DataSource.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_BY_LOGIN)){
preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            users=readAll(resultSet);

        }catch(SQLException e){}
        if(users.size()==0){
            userTmp=null;
        }else{
            userTmp=users.get(0);
        }

        return userTmp;
    }
}
