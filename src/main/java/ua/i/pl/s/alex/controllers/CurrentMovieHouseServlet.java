package ua.i.pl.s.alex.controllers;

import ua.i.pl.s.alex.dto.*;
import ua.i.pl.s.alex.model.Times;
import ua.i.pl.s.alex.service.impl.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 04.04.2017.
 */
@WebServlet(name = "CurrentMovieHouseServlet", urlPatterns = "/current-movie-house")
public class CurrentMovieHouseServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String row_movie_house_id = request.getParameter("movie-house");
        Object movieObj_id = request.getSession().getAttribute("movie_id");
        Object date = request.getSession().getAttribute("date");
        int movie_house_id=Integer.parseInt(row_movie_house_id);
        MovieHouseDTO currentMovieHouse = MovieHouseServiceImpl.getInstance().getById(movie_house_id);
        Date currentDate=(Date)date;
        List<HallDTO> hallAllByMovieHouseId = HallServiceImpl.getInstance().getAllByMovieHouseId(movie_house_id);
        List<SeanceDTO> seanceAll =SeanceServiceImpl.getInstance().getAllByMovieHouseIdAndDate(movie_house_id, currentDate, currentDate);
        int movie_id=0;
        MovieServiceImpl movieService = MovieServiceImpl.getInstance();

//        for(SeanceDTO seance:seanceAll){
//            System.out.println(seance);
//        }

        for(SeanceDTO seance:seanceAll){
            MovieDTO movieById = movieService.getById(seance.getMovieId());
            seance.setMovieTitle(movieById.getName());
        }
        for(SeanceDTO seance:seanceAll){
            TimesDTO times= TimesServiceImpl.getInstance().getById(seance.getCurrentSeanceTime());
            seance.setCurrentTime(times.getTime());
        }

if(movieObj_id==null){

    request.setAttribute("seanceAll", seanceAll);

}else{
    movie_id=(Integer)movieObj_id;
    List<SeanceDTO> seanceAllTmp=new ArrayList<>();
for(SeanceDTO seance:seanceAll){
     if(seance.getMovieId()==movie_id){
         seanceAllTmp.add(seance);
     }
    }
    seanceAll=seanceAllTmp;
    request.setAttribute("seanceAll", seanceAll);
}


        request.setAttribute("hallAllByMovieHouseId", hallAllByMovieHouseId);
        request.getSession().setAttribute("currentMovieHouse", currentMovieHouse);
//        request.setAttribute("movie_id", movie_id);
        request.getRequestDispatcher("/pages/common/current-movie-house.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
doPost(request, response);
    }
}
