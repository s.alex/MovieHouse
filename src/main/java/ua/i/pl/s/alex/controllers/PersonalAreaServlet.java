package ua.i.pl.s.alex.controllers;

import ua.i.pl.s.alex.dto.UserDTO;
import ua.i.pl.s.alex.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Created by A Sosnovskyi on 15.04.2017.
 */
@WebServlet(name = "PersonalAreaServlet", urlPatterns = "/personal-area")
public class PersonalAreaServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String change = request.getParameter("change");
        UserDTO userByLogin=(UserDTO) request.getSession().getAttribute("userByLogin");



        String userLogin = request.getParameter("userLogin");
        //=========================не удобно=========================================
//        Map<String, String[]> parameterMap = request.getParameterMap();
//        if(parameterMap.containsKey("userLogin")){
//            String[] logins = parameterMap.get("userLogin");
//            System.out.println(logins[0]);
//        }
        //======================================================================
        if(userLogin!=null&&userLogin!=""){
            UserDTO newUser=new UserDTO();
            newUser.setId(userByLogin.getId());
            newUser.setUserName(request.getParameter("userName"));
            newUser.setUserSurname(request.getParameter("userSurname"));
            newUser.setUserLogin(userLogin);
            newUser.setUserEmail(request.getParameter("userEmail"));
            newUser.setUserPassword(userByLogin.getUserPassword());
            newUser.setPermissions(userByLogin.getPermissions());
            UserServiceImpl.getInstance().update(newUser);
            request.getSession().setAttribute("userByLogin", null);
            request.getSession().setAttribute("userByLogin", newUser);
        }
        String newPassword = request.getParameter("newPassword");

        if(newPassword!=null&&newPassword!=""){
            String oldPassword = request.getParameter("oldPassword");
            String confirmPassword = request.getParameter("confirmPassword");
            if(oldPassword.equals(userByLogin.getUserPassword())){
                if(confirmPassword.equals(newPassword)){
                    UserDTO newUser=new UserDTO();
                    newUser.setUserName(userByLogin.getUserName());
                    newUser.setUserSurname(userByLogin.getUserSurname());
                    newUser.setUserLogin(userByLogin.getUserLogin());
                    newUser.setUserEmail(userByLogin.getUserEmail());
                    newUser.setUserDiscount(userByLogin.getUserDiscount());
                    newUser.setPermissions(userByLogin.getPermissions());
                    newUser.setUserPassword(newPassword);
                    UserServiceImpl.getInstance().update(newUser);
                }else{
                    request.setAttribute("message","пароль и подтверждение пароля не совпадают");
                }
            }else{
                request.setAttribute("message","Неверный пароль");
            }
        }
        request.getRequestDispatcher("pages/common/personal-area.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
