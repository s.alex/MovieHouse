package ua.i.pl.s.alex.helper;

import ua.i.pl.s.alex.dto.MovieDTO;
import ua.i.pl.s.alex.dto.SeanceDTO;
import ua.i.pl.s.alex.service.impl.MovieServiceImpl;
import ua.i.pl.s.alex.service.impl.SeanceServiceImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by A Sosnovskyi on 12.04.2017.
 */
public class FilmByDateGetter {
    public FilmByDateGetter() {
    }
    public List<MovieDTO> getFilmsByDate(Date date1, Date date2){
        SeanceServiceImpl seanceInstance = SeanceServiceImpl.getInstance();
        List<SeanceDTO> allSeancesByDate = seanceInstance.getAllByDate(date1, date2);
        Set<Integer> filmsID = new HashSet<>();
        for (SeanceDTO seanceDTO : allSeancesByDate) {
            filmsID.add(seanceDTO.getMovieId());
        }
        MovieServiceImpl movieInstance = MovieServiceImpl.getInstance();
        List<MovieDTO> movieDTOs = new ArrayList<>();
        for (int entry : filmsID) {
            MovieDTO movieById = movieInstance.getById(entry);
            movieDTOs.add(movieById);
        }
        return movieDTOs;
    }
}
