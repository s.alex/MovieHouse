package ua.i.pl.s.alex.service.impl;


import ua.i.pl.s.alex.dao.DaoFactory;
import ua.i.pl.s.alex.dao.api.Dao;
import ua.i.pl.s.alex.dao.impl.SeanceDaoImpl;
import ua.i.pl.s.alex.dao.impl.SeatDaoImpl;
import ua.i.pl.s.alex.dto.SeatDTO;
import ua.i.pl.s.alex.mapper.BeanMapper;
import ua.i.pl.s.alex.model.Seat;
import ua.i.pl.s.alex.service.api.Service;

import java.util.List;

/**
 * Created by Admin on 21.03.2017.
 */
public class SeatServiceImpl implements Service<Integer, SeatDTO> {
    private static SeatServiceImpl service;
    private Dao<Integer, Seat> seatDao;
    private BeanMapper beanMapper;
    private SeanceDaoImpl seanceDao;
    private SeatDaoImpl seatDaoImpl;

    private SeatServiceImpl() {
        seatDao = DaoFactory.getInstance().getSeatDao();
        beanMapper = BeanMapper.getInstance();
        seatDaoImpl = SeatDaoImpl.getInstance();
    }

    public static synchronized SeatServiceImpl getInstance() {
        if (service == null) {
            service = new SeatServiceImpl();
        }

        return service;
    }


    @Override
    public List<SeatDTO> getAll() {
        List<Seat> seatList = seatDao.getAll();
        List<SeatDTO> seatDTOs = beanMapper.listMapToList(seatList, SeatDTO.class);
        return seatDTOs;
    }

    @Override
    public SeatDTO getById(Integer key) {
        Seat seat = seatDao.getById(key);
        SeatDTO seatDTO = beanMapper.singleMapper(seat, SeatDTO.class);
        return seatDTO;
    }

    @Override
    public void save(SeatDTO entity) {
        Seat seat = beanMapper.singleMapper(entity, Seat.class);
        seatDao.save(seat);
    }
    public void saveAll(List<SeatDTO> entity) {
        List<Seat> seat = beanMapper.listMapToList(entity, Seat.class);
        seatDaoImpl.saveAll(seat);
    }
    @Override
    public void delete(Integer key) {
        seatDao.delete(key);
    }

    @Override
    public void update(SeatDTO entity) {
        Seat seat = beanMapper.singleMapper(entity, Seat.class);
        seatDao.update(seat);
    }

    public List<SeatDTO> getAllByHallId(int key) {
//        System.out.println("getAllByHallId"+key);
        List<Seat> seatList =seatDaoImpl.readAllByHallId(key);
        List<SeatDTO> seatDTOs = beanMapper.listMapToList(seatList, SeatDTO.class);
        return seatDTOs;
    }

    public List<SeatDTO> getAllBySeanceId(int key) {
       // System.out.println("getAllBySeanceId"+key);
        List<Seat> seatList =seatDaoImpl.readAllBySeanceId(key);
//        for(Seat seat:seatList){
//            System.out.println(seat);
//        }
        List<SeatDTO> seatDTOs = beanMapper.listMapToList(seatList, SeatDTO.class);
        return seatDTOs;
    }
}
