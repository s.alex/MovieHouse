package ua.i.pl.s.alex.controllers;


import ua.i.pl.s.alex.dto.MovieDTO;
import ua.i.pl.s.alex.dto.MovieHouseDTO;
import ua.i.pl.s.alex.dto.SeanceDTO;
import ua.i.pl.s.alex.dto.UserDTO;
import ua.i.pl.s.alex.helper.FilmByDateGetter;
import ua.i.pl.s.alex.service.impl.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Admin on 01.04.2017.
 */
//name = "MainServlet", urlPatterns =
@WebServlet(name = "MainServlet", urlPatterns = "/Main")
public class MainServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String exit = request.getParameter("exit");
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        if (exit != null) {
            request.getSession().removeAttribute("userByLogin");
        }


        if (login != null && login != "" && password != null && password != "") {
            UserDTO userByLogin = UserServiceImpl.getInstance().getByLogin(login);

            if (userByLogin == null) {
                request.setAttribute("userByLogin", userByLogin);
            } else {
                if (userByLogin.getUserPassword().equals(password)) {
                    request.getSession().setAttribute("userByLogin", userByLogin);

                } else {
                    request.setAttribute("userByLogin", null);
                }

            }

        }


        Object newDate = request.getParameter("date");
        Date dateBegin, dateEnd;

        if (newDate == null) {
            dateBegin = new Date(System.currentTimeMillis());
        } else {
            dateBegin = Date.valueOf((String) newDate);
        }
//--------------------------------поместить в хелпер-----------------------------------------------------------------
//        SeanceServiceImpl seanceInstance = SeanceServiceImpl.getInstance();
//
//        List<SeanceDTO> allSeancesByDate = seanceInstance.getAllByDate(date, date);
//        Set<Integer> filmsID = new HashSet<>();
//        for (SeanceDTO seanceDTO : allSeancesByDate) {
//            filmsID.add(seanceDTO.getMovieId());
//        }
//        MovieServiceImpl movieInstance = MovieServiceImpl.getInstance();
//        List<MovieDTO> movieDTOs = new ArrayList<>();
//        for (int entry : filmsID) {
//            MovieDTO movieById = movieInstance.getById(entry);
//            movieDTOs.add(movieById);
//        }
     //----------------------------------------------------------------------------------------------------
        FilmByDateGetter filmHelper=new FilmByDateGetter();
        List<MovieDTO> movieDTOs=filmHelper.getFilmsByDate(dateBegin, dateBegin);
        //-------------------------------------------------------------------------------------------------
        request.getSession().setAttribute("date", dateBegin);
        request.setAttribute("movieDTOs", movieDTOs);
        request.getRequestDispatcher("/pages/common/actuals.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);//interesting ))
    }
}
