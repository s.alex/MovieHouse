package ua.i.pl.s.alex.service.impl;



import ua.i.pl.s.alex.dao.DaoFactory;
import ua.i.pl.s.alex.dao.api.Dao;
import ua.i.pl.s.alex.dao.impl.SeanceDaoImpl;
import ua.i.pl.s.alex.dto.SeanceDTO;
import ua.i.pl.s.alex.mapper.BeanMapper;
import ua.i.pl.s.alex.model.Seance;
import ua.i.pl.s.alex.service.api.Service;

import java.sql.Date;
import java.util.List;

/**
 * Created by Admin on 21.03.2017.
 */
public class SeanceServiceImpl implements Service<Integer, SeanceDTO> {
    private static SeanceServiceImpl service;
    private Dao<Integer, Seance> seanceDao;
    private BeanMapper beanMapper;

    private SeanceServiceImpl() {
        seanceDao = DaoFactory.getInstance().getSeanceDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized SeanceServiceImpl getInstance() {
        if (service == null) {
            service = new SeanceServiceImpl();
        }

        return service;
    }

    @Override
    public List<SeanceDTO> getAll() {
        List<Seance> seanceList = seanceDao.getAll();
        List<SeanceDTO> seanceDTOs = beanMapper.listMapToList(seanceList, SeanceDTO.class);
        return seanceDTOs;
    }

    @Override
    public SeanceDTO getById(Integer key) {
        Seance seance = seanceDao.getById(key);
        SeanceDTO seanceDTO = beanMapper.singleMapper(seance, SeanceDTO.class);
        return seanceDTO;
    }

    @Override
    public void save(SeanceDTO entity) {
        Seance seance = beanMapper.singleMapper(entity, Seance.class);
        seanceDao.save(seance);
    }

    @Override
    public void delete(Integer key) {
        seanceDao.delete(key);
    }

    @Override
    public void update(SeanceDTO entity) {
        Seance seance = beanMapper.singleMapper(entity, Seance.class);
        seanceDao.update(seance);
    }
    public List<SeanceDTO> getAllByDate(Date dateBegin, Date dateEnd){
        List<Seance> seanceList = SeanceDaoImpl.getInstance().readAllByDate(dateBegin, dateEnd);
        List<SeanceDTO> seanceDTOs = beanMapper.listMapToList(seanceList, SeanceDTO.class);
        return seanceDTOs;
    }
    public List<SeanceDTO> getAllByMovieId(int key){
        List<Seance> seanceList = SeanceDaoImpl.getInstance().readAllByMovieId(key);
        List<SeanceDTO> seanceDTOs = beanMapper.listMapToList(seanceList, SeanceDTO.class);
        return seanceDTOs;
    }

    public List<SeanceDTO> getAllByMovieHouseId(int key){
        List<Seance> seanceList = SeanceDaoImpl.getInstance().readAllByMovieId(key);
        List<SeanceDTO> seanceDTOs = beanMapper.listMapToList(seanceList, SeanceDTO.class);
        return seanceDTOs;
    }
    public List<SeanceDTO> getAllByMovieHouseIdAndDate(int key, Date dateBegin, Date dateEnd){
        List<Seance> seanceList = SeanceDaoImpl.getInstance().readAllByMovieHouseIdAndDate(key, dateBegin,  dateEnd);
        List<SeanceDTO> seanceDTOs = beanMapper.listMapToList(seanceList, SeanceDTO.class);
        return seanceDTOs;
    }
}
