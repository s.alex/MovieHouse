package ua.i.pl.s.alex.controllers;

import ua.i.pl.s.alex.dto.UserDTO;
import ua.i.pl.s.alex.model.User;
import ua.i.pl.s.alex.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Admin on 09.04.2017.
 */
@WebServlet(name = "RegistrationServlet", urlPatterns = "/registration")
public class RegistrationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        UserDTO userDTO=null;
        UserDTO userByLogin =null;
        UserServiceImpl instance = UserServiceImpl.getInstance();

        if (name != null && name != "" && surname != null && surname != "" && login != null && login != "" &&
                password != null && password != "" && email != null && email != "") {
            userDTO=new UserDTO();
            userDTO.setUserName(name);
            userDTO.setUserSurname(surname);
            userDTO.setUserLogin(login);
            userDTO.setUserPassword(password);
            userDTO.setUserEmail(email);
            userDTO.setPermissions(0);
            userByLogin = instance.getByLogin(login);
            if(userByLogin!=null){
                response.setHeader("exist", login);
            }else{
                instance.save(userDTO);
                request.getSession().setAttribute("userByLogin", userDTO);
                request.getRequestDispatcher("/pages/common/actuals.jsp").forward(request, response);
            }
        }

        request.getRequestDispatcher("/pages/common/registration.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
