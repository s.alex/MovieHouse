package ua.i.pl.s.alex.dto;

import lombok.Getter;
import lombok.Setter;
import ua.i.pl.s.alex.model.Entity;

import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by Admin on 17.03.2017.
 */
@Getter
@Setter
public class SeanceDTO extends Entity<Integer> {
    private Date dayBegin;
    private Date dayEnd;
    private int currentSeanceTime;
    private Time currentTime;
  //  private List<Date> timesOfShowTable;
    private List<Integer> seats;
    private int movieId;
    private String movieTitle;
    private int hallId;
    private int movieHouseId;
    private int price;

    public SeanceDTO(Date dayBegin, Date dayEnd , int currentSeanceTime, int movieId, int hallId, int movieHouseId, int price) {
        this.dayBegin = dayBegin;
        this.dayEnd = dayEnd;
        this.currentSeanceTime=currentSeanceTime;
        this.movieId = movieId;
        this.hallId = hallId;
        this.movieHouseId=movieHouseId;
        this.price = price;
    }

    public SeanceDTO() {
    }

    @Override
    public String toString() {
        return "Seance{" +
                "id="+getId()+
                ", dayBegin=" + dayBegin +
                ", dayEnd=" + dayEnd +
                ", currentSeanceTime=" + currentSeanceTime +
                ", movieId=" + movieId +
                ", hallId=" + hallId +
                ", movieHouseId=" + movieHouseId +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SeanceDTO)) return false;
//        if (!super.equals(o)) return false;
        SeanceDTO seanceDTO = (SeanceDTO) o;
        return getCurrentSeanceTime() == seanceDTO.getCurrentSeanceTime() &&
                getMovieId() == seanceDTO.getMovieId() &&
                getHallId() == seanceDTO.getHallId() &&
                getMovieHouseId() == seanceDTO.getMovieHouseId()  &&
                Objects.equals(getDayBegin(), seanceDTO.getDayBegin()) &&
                Objects.equals(getDayEnd(), seanceDTO.getDayEnd());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getDayBegin(), getDayEnd(), getCurrentSeanceTime(), getMovieId(), getHallId(), getMovieHouseId());
    }
}
