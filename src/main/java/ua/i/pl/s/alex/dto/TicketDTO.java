package ua.i.pl.s.alex.dto;

import lombok.Getter;
import lombok.Setter;
import ua.i.pl.s.alex.model.Entity;

import java.util.Date;

/**
 * Created by Admin on 17.03.2017.
 */
@Getter
@Setter
public class TicketDTO extends Entity<Integer> {
    private  int movieHouseName; //
    private  Date timeOfSell;  //
    private  int userId;   //
    private  int hall;   //
    private  int seanceId;  //
    private  int seatRow; //
    private   int seatPosition;
    private  int price;

    public TicketDTO(int movieHouseName, Date timeOfSell,
                     int userId, int hall, int seanceId, int seatRow, int seatPosition, int price) {
        this.movieHouseName = movieHouseName;
        this.timeOfSell = timeOfSell;
        this.userId = userId;
        this.hall = hall;
        this.seanceId = seanceId;
        this.seatRow = seatRow;
        this.seatPosition = seatPosition;
        this.price = price;
    }

    public TicketDTO() {

    }

    @Override
    public String toString() {
        return "TicketDTO{" +
                "movieHouseName=" + movieHouseName +
                ", timeOfSell=" + timeOfSell +
                ", userId=" + userId +
                ", hall=" + hall +
                ", seanceId=" + seanceId +
                ", seatRow=" + seatRow +
                ", seatPosition=" + seatPosition +
                ", price=" + price +
                '}';
    }
}
