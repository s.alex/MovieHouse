package ua.i.pl.s.alex.controllers;

import ua.i.pl.s.alex.dao.impl.TimeDaoImpl;
import ua.i.pl.s.alex.dto.*;
import ua.i.pl.s.alex.model.Movie;
import ua.i.pl.s.alex.model.Ticket;
import ua.i.pl.s.alex.model.Times;
import ua.i.pl.s.alex.service.impl.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Admin on 07.04.2017.
 */
@WebServlet(name = "CheckoutServlet", urlPatterns = "/seats")
public class CheckoutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String seanceId = request.getParameter("seance_id");
        int id = Integer.valueOf(seanceId);
        Object userByLogin = request.getSession().getAttribute("userByLogin");
        UserDTO user=null;
        if(userByLogin!=null){
            user=(UserDTO)userByLogin;}
        String row1 = request.getParameter("row");
        String seat = request.getParameter("seat");
        if(row1!=null&&user!=null){
            int row =Integer.parseInt(row1);
            int seatNumber =Integer.parseInt(seat);
//            System.out.println("row="+row+" seatNumber="+seatNumber);
            TicketDTO ticket=new TicketDTO();
           ticket.setSeatRow(row);
            ticket.setSeatPosition(seatNumber);
            ticket.setSeanceId(id);
            ticket.setUserId(user.getId());
            Object list = request.getSession().getAttribute("ticketList");
            int size=0;
            if(list==null){
               size=1;
                List<TicketDTO> ticketList=new ArrayList<>();
                ticketList.add(ticket);
                request.getSession().setAttribute("ticketList", ticketList);
                request.setAttribute("size", size);
            }else{
                List<TicketDTO> ticketList=(List<TicketDTO>)list;

                ticketList.add(ticket);
                size=ticketList.size();
                request.setAttribute("size", size);
                request.getSession().setAttribute("ticketList", ticketList);
            }
        }

        SeanceDTO currentSeance = SeanceServiceImpl.getInstance().getById(id);
        HallDTO currentHall = HallServiceImpl.getInstance().getById(currentSeance.getHallId());
        int[][] structureHall = new int[currentHall.getRows()][currentHall.getRowSeats()];

//        System.out.println(currentHall);
        Times currentTime = TimeDaoImpl.getInstance().readById(currentSeance.getCurrentSeanceTime());
        MovieHouseDTO currentMovieHouse = MovieHouseServiceImpl.getInstance().getById(currentSeance.getMovieHouseId());
        MovieDTO currentMovie = MovieServiceImpl.getInstance().getById(currentSeance.getMovieId());
        List<SeatDTO> soldSeats = SeatServiceImpl.getInstance().getAllBySeanceId(currentSeance.getId());

        for(int row=0; row<currentHall.getRows(); row++){
            for(int calls=0; calls<currentHall.getRowSeats(); calls++){
                structureHall[row][calls]=1;

            }
        }
        for(SeatDTO soldseat: soldSeats){
            int row=soldseat.getSeatRow();
            int pos=soldseat.getSeatPosition();
            structureHall[row-1][pos-1]=0;
          //  System.out.println(soldseat.getId());
        }
        for(int row=0; row<currentHall.getRows(); row++){
            for(int calls=0; calls<currentHall.getRowSeats(); calls++){
             //   System.out.println(structureHall[row][calls]);

            }
        }
        request.getSession().setAttribute("user", user);
        request.getSession().setAttribute("structureHall", structureHall);
        request.getSession().setAttribute("currentSeance", currentSeance);
        request.getSession().setAttribute("currentHall", currentHall);
        request.getSession().setAttribute("currentTime", currentTime);
        request.getSession().setAttribute("currentMovieHouse", currentMovieHouse);
        request.getSession().setAttribute("currentMovie", currentMovie);
        request.getSession().setAttribute("soldSeats", soldSeats);
        request.getRequestDispatcher("/pages/common/seats.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
