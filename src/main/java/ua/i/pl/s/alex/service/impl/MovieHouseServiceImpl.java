package ua.i.pl.s.alex.service.impl;


import ua.i.pl.s.alex.dao.DaoFactory;
import ua.i.pl.s.alex.dao.api.Dao;
import ua.i.pl.s.alex.dao.impl.MovieHouseDaoImpl;
import ua.i.pl.s.alex.dto.HallDTO;
import ua.i.pl.s.alex.dto.MovieHouseDTO;
import ua.i.pl.s.alex.mapper.BeanMapper;
import ua.i.pl.s.alex.model.MovieHouse;
import ua.i.pl.s.alex.service.api.Service;

import java.util.List;

/**
 * Created by Admin on 21.03.2017.
 */
public class MovieHouseServiceImpl implements Service<Integer, MovieHouseDTO> {
    private static MovieHouseServiceImpl service;
    private Dao<Integer, MovieHouse> movieHouceDao;
    private BeanMapper beanMapper;

    private MovieHouseServiceImpl() {
        movieHouceDao = DaoFactory.getInstance().getMovieHouseDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized MovieHouseServiceImpl getInstance() {
        if (service == null) {
            service = new MovieHouseServiceImpl();
        }

        return service;
    }

    @Override
    public List<MovieHouseDTO> getAll() {
        List <MovieHouse> movieHouseList=movieHouceDao.getAll();

//        for (MovieHouse movieHouse: movieHouseList){
//           List<HallDTO> hallList= HallServiceImpl.getInstance().getAllByMovieHouseId(movieHouse.getId());
//
//
//            int i=0;
//            int[] halls=new int[movieHouse.getHallsQuantity()];
//
//
//            for(HallDTO hall:hallList){
//
//                halls[i]=hall.getId();
//                i++;
//            }
//
//            movieHouse.setMovieHalls(halls);
//
//        }

        List<MovieHouseDTO> movieHouseDTOList= beanMapper.listMapToList(movieHouseList, MovieHouseDTO.class);

        return movieHouseDTOList;
    }

    @Override
    public MovieHouseDTO getById(Integer key) {
        MovieHouse movieHouse=movieHouceDao.getById(key);
        MovieHouseDTO movieHouseDTO=BeanMapper.singleMapper(movieHouse, MovieHouseDTO.class);
        return movieHouseDTO;
    }

    @Override
    public void save(MovieHouseDTO entity) {
        MovieHouse movieHouse=BeanMapper.singleMapper(entity, MovieHouse.class);
        movieHouceDao.save(movieHouse);
    }

    @Override
    public void delete(Integer key) {
        movieHouceDao.delete(key);

    }

    @Override
    public void update(MovieHouseDTO entity) {
        MovieHouse movieHouse=BeanMapper.singleMapper(entity, MovieHouse.class);
        movieHouceDao.update(movieHouse);
    }

    public MovieHouseDTO getByName(String name){
        MovieHouse movieHouse = MovieHouseDaoImpl.getInstance().getByName(name);
        MovieHouseDTO movieHouseDTO=beanMapper.singleMapper(movieHouse, MovieHouseDTO.class);
        return movieHouseDTO;
    }
}
