package ua.i.pl.s.alex.controllers;

import ua.i.pl.s.alex.dto.MovieDTO;
import ua.i.pl.s.alex.helper.FilmByDateGetter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

/**
 * Created by A Sosnovskyi on 14.04.2017.
 */
@WebServlet(name = "SoonServlet", urlPatterns = "/soon")
public class SoonServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Date dateBegin, date;

        date = new Date(System.currentTimeMillis());
        dateBegin = new Date(System.currentTimeMillis() + 3 * 7 * 24 * 60 * 60 * 1000);
        FilmByDateGetter filmHelper = new FilmByDateGetter();
        List<MovieDTO> movieDTOs = filmHelper.getFilmsByDate(dateBegin, dateBegin);
        request.setAttribute("movieDTOs", movieDTOs);
        request.getSession().setAttribute("date", date);
        request.getRequestDispatcher("/pages/common/soon.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
