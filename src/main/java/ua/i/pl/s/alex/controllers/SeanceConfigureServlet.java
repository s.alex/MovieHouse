package ua.i.pl.s.alex.controllers;

import ua.i.pl.s.alex.dto.*;
import ua.i.pl.s.alex.helper.FilmByDateGetter;
import ua.i.pl.s.alex.service.impl.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by A Sosnovskyi on 12.04.2017.
 */
@WebServlet(name = "SeanceConfigureServlet", urlPatterns = "/admin/seance-configure")
public class SeanceConfigureServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        Date dateBegin;
        dateBegin = new Date(System.currentTimeMillis());
        request.setAttribute("date", dateBegin);
        String newDate = request.getParameter("date");
        if(newDate!=null){
            dateBegin=  Date.valueOf(newDate);
            request.setAttribute("date", dateBegin);
        }
        //----------------------------------------------------------------------------------------------------------
        FilmByDateGetter filmHelper=new FilmByDateGetter();
       // List<MovieDTO> movieDTOs=filmHelper.getFilmsByDate(dateBegin, dateBegin);
        List<MovieDTO> movieDTOs= MovieServiceImpl.getInstance().getAll();
        Map<Integer, String> movieMap=new HashMap<>();
        for(MovieDTO movie: movieDTOs){
            movieMap.put(movie.getId(), movie.getName());
        }
        request.setAttribute("movieMap", movieMap);
        String currentMovie = request.getParameter("currentMovie");
        if(currentMovie!=null){
            request.getSession().setAttribute("currentMovie", Integer.parseInt(currentMovie));
        }
        //------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
        List<MovieHouseDTO> movieHouseDTOs= MovieHouseServiceImpl.getInstance().getAll();
        Map<Integer, String> houseMap=new HashMap<>();
        for(MovieHouseDTO house: movieHouseDTOs){
            houseMap.put(house.getId(), house.getName());
        }
        request.setAttribute("houseMap", houseMap);
        //------------------------------------------------------------------------------------------------------
        SeanceServiceImpl seanceInstance = SeanceServiceImpl.getInstance();
        List<SeanceDTO> allSeancesByDate = seanceInstance.getAllByDate(dateBegin, dateBegin);
        //---------------------------------------------------------------------------------------------------------
        List<TimesDTO> timesDTOs = TimesServiceImpl.getInstance().getAll();
        Map<Integer, String> timeMap=new HashMap<>();
        String currentTime = request.getParameter("currentTime");
        String newTime = request.getParameter("newTime");
        if(newTime!=null){
            TimesDTO timeDTO=new TimesDTO();
//            System.out.println("newTime"+newTime);
            timeDTO.setTime(Time.valueOf(newTime.concat(":00")));
//            System.out.println(timesDTO.getTime());
            List<TimesDTO> allTimes = TimesServiceImpl.getInstance().getAll();
            if(!allTimes.contains(timeDTO)){
                TimesServiceImpl.getInstance().save(timeDTO);
            }
        }
        for(TimesDTO time: timesDTOs){
            timeMap.put(time.getId(), time.getTime().toString());
        }
        if(currentTime!=null){
            int key=Integer.parseInt(currentTime);
            request.getSession().setAttribute("currentTime", key);
        }
        request.setAttribute("timeMap", timeMap);
        //----------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------
        List<HallDTO> hallDTOs= HallServiceImpl.getInstance().getAll();
        Map<Integer, String> hallMap=new HashMap<>();
        for (HallDTO hallDTO : hallDTOs) {
            hallMap.put(hallDTO.getId(), hallDTO.getName());
        }
        request.setAttribute("hallMap", hallMap);
        String movieHouse = request.getParameter("movieHouse");
        String movieHall = request.getParameter("movieHall");
        if(movieHouse!=null){
            int key=Integer.parseInt(movieHouse);
            hallDTOs=HallServiceImpl.getInstance().getAllByMovieHouseId(key);
            request.getSession().setAttribute("movieHouse", key);
        }
if(movieHall!=null){
    request.getSession().setAttribute("movieHall", Integer.parseInt(movieHall));
}
        String price = request.getParameter("price");
        if(price!=null){
            SeanceDTO newSeance=new SeanceDTO();
            int movieHouseInt = Integer.parseInt(request.getParameter("movieHouse")) ;
            int movieHallInt = Integer.parseInt(request.getParameter("movieHall")) ;
            int currentTimeInt = Integer.parseInt(request.getParameter("currentTime")) ;
            int currentMovieInt = Integer.parseInt(request.getParameter("currentMovie")) ;
            newSeance.setHallId(movieHallInt);
            newSeance.setMovieHouseId(movieHouseInt);
            newSeance.setMovieId(currentMovieInt);
            newSeance.setCurrentSeanceTime(currentTimeInt);
            newSeance.setDayBegin(Date.valueOf(request.getParameter("dayBegin")));
            newSeance.setDayEnd(Date.valueOf(request.getParameter("dayEnd")));
            newSeance.setPrice(Integer.parseInt(price));
            List<SeanceDTO> allSeances = SeanceServiceImpl.getInstance().getAll();
            if(!allSeances.contains(newSeance)){
                SeanceServiceImpl.getInstance().save(newSeance);
                request.getSession().removeAttribute("movieHouse");
                request.getSession().removeAttribute("movieHall");
                request.getSession().removeAttribute("currentTime");
                request.getSession().removeAttribute("currentMovie");
            }
        }
        //-----------------------------------------------------------------------------------------------------------
        request.setAttribute("movieDTOs", movieDTOs);
        request.setAttribute("hallDTOs", hallDTOs);
        request.setAttribute("timesDTOs", timesDTOs);

        request.setAttribute("allSeancesByDate", allSeancesByDate);
        request.setAttribute("movieHouseDTOs", movieHouseDTOs);
        request.getRequestDispatcher("/pages/admin/seance-configure.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
