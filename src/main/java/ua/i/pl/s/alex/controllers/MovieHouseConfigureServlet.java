package ua.i.pl.s.alex.controllers;

import ua.i.pl.s.alex.dto.HallDTO;
import ua.i.pl.s.alex.dto.MovieDTO;
import ua.i.pl.s.alex.dto.MovieHouseDTO;
import ua.i.pl.s.alex.service.impl.HallServiceImpl;
import ua.i.pl.s.alex.service.impl.MovieHouseServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by A Sosnovskyi on 12.04.2017.
 */
@WebServlet(name = "MovieHouseConfigureServlet", urlPatterns = "/admin/movie-house-configure")
public class MovieHouseConfigureServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String houseToDelete = request.getParameter("house");

        if(houseToDelete!=null){
            MovieHouseServiceImpl.getInstance().delete(Integer.parseInt(houseToDelete));
        }
        String houseTitle = request.getParameter("houseTitle");

        if(houseTitle!=null){
            String houseHalls = request.getParameter("houseHalls");
            MovieHouseDTO house=new MovieHouseDTO();
            house.setName(houseTitle);
            house.setHallsQuantity(Integer.parseInt(houseHalls));
            MovieHouseServiceImpl.getInstance().save(house);
//            MovieHouseDTO houseByName = MovieHouseServiceImpl.getInstance().getByName(houseTitle);
//            request.setAttribute("houseByName", houseByName);
        }

        String houseIdStr = request.getParameter("houseId");
        if(houseIdStr!=null){
            List<HallDTO> hallDTOs=HallServiceImpl.getInstance().getAllByMovieHouseId(Integer.parseInt(houseIdStr));
            request.setAttribute("hallDTOs", hallDTOs);
            request.setAttribute("houseId", houseIdStr);
        }

/*Insert new hall*/

        String hallName = request.getParameter("hallName");
        if(hallName!=null){
            int rows = Integer.parseInt(request.getParameter("rows"));
            int rowSeats = Integer.parseInt(request.getParameter("rowSeats"));
            int houseId =Integer.parseInt(request.getParameter("houseId")) ;
            HallDTO hall=new HallDTO();
            hall.setName(hallName);
            hall.setRows(rows);
            hall.setRowSeats(rowSeats);
            hall.setSeatsQuantity(rows*rowSeats);
            hall.setMovieHouseId(houseId);
            HallServiceImpl.getInstance().save(hall);
        }

        List<MovieHouseDTO> houseDTOs = MovieHouseServiceImpl.getInstance().getAll();

        List<HallDTO> hallDTOs = HallServiceImpl.getInstance().getAll();

        Map<Integer,HallDTO> hallMap=new HashMap<>();
        for (HallDTO hallDTO: hallDTOs){
            hallMap.put(hallDTO.getId(), hallDTO);
        }

        request.setAttribute("houseDTOs", houseDTOs);
        request.setAttribute("hallMap", hallMap);
        request.getRequestDispatcher("/pages/admin/movie-house-configure.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
doPost(request, response);
    }
}
