package ua.i.pl.s.alex.dto;

import lombok.Getter;
import lombok.Setter;
import ua.i.pl.s.alex.model.Entity;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created by Admin on 16.03.2017.
 */
@Getter
@Setter
public class MovieHouseDTO extends Entity<Integer> implements Comparable{
    private String name;
    private int hallsQuantity;
    private int[] movieHalls;


    public MovieHouseDTO() {
    }

    public MovieHouseDTO(String name, int hallsQuantity) {
        this.name = name;
        this.hallsQuantity = hallsQuantity;
        this.movieHalls = new int[hallsQuantity];
    }

public void setMovieHalls(int[] halls){
    this.movieHalls=halls;
}
    @Override
    public String toString() {
        return "MovieHouse{" +
                "movieHouseId="+this.getId()+
                " name='" + name + '\'' +
                ", halls=" + Arrays.toString(movieHalls) +
                ", hallsQuantity=" + hallsQuantity +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        return this.getId()-((MovieHouseDTO)o).getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MovieHouseDTO)) return false;
//        if (!super.equals(o)) return false;
        MovieHouseDTO that = (MovieHouseDTO) o;
        return getHallsQuantity() == that.getHallsQuantity() &&
                Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(14*getId()+  getHallsQuantity());
    }
}

